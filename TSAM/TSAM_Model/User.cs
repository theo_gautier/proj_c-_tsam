﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace TSAM.Model
{
    /**\brief This is the class for all the user informations and actions
     * 
     * 
     */
    public class User
    {
        #region private attributes
        
        private int id;
        private string mail;
        private bool isAdminAccount;

        #endregion

        #region constructor
        /**\brief the constructor
         * 
         */
        public User()
        {
            
        }
        #endregion

        #region accessors
        /**\brief the mail of the user
         * 
         * 
         */
        public string Mail
        {
            get { return mail; }
        }

        /**\brief false if this is a user account and true if it's an administrator
         * 
         * 
         */
        public bool IsAdminAccount { get => isAdminAccount;}
        /**
         *\brief the id of the user in the database
         * 
         */
        public int Id { get => id; }

        #endregion

        #region public methodes
        /**\brief This is the Login method, if it succeed it will set the attributes
         *\param mail the mail of the user  
         *\param password   the password in clear text
         */
        public void Login(string mail, string password)
        {
            bool correctLogin = IsLoginCorrect(mail, password);

            if (correctLogin)
            {
                List<List<object>> queryResut;
                string query = "SELECT ID,IsAdminAccount From users where userEmailAddress ='" + mail.ToLower() + "';";
                BdConnector bdConnector = new BdConnector();
                queryResut = bdConnector.SelectQuery(query);

                this.id = Convert.ToInt32(queryResut[0][0]);
                this.isAdminAccount = Convert.ToBoolean(queryResut[0][1]);
                this.mail = mail;
            }
            else
            {
                throw new InvalidCreditentialsException();
            }
        }

        /**\brief The Register method, it do not log the user automaticaly
         *\param email the mail of the user  
         *\param givenpassword   the password in clear text
         */
        public void Register(string email, string givenPassword)
        {
            bool alreadyExists = IsAccountExisting(email.ToLower());

            string givenPasswordHashed = GetHashString(givenPassword);

            


            if (!alreadyExists)
            {
                string query = "INSERT INTO users (`UserEmailAddress`, `UserHashPsw`) VALUES ('"+ email.ToLower()+ "','"+ givenPasswordHashed + "')";
                BdConnector bdConnector = new BdConnector();
                bdConnector.InsertQuery(query);
            }
            else
            {
                throw new InvalidCreditentialsException();
            }

        }
        #endregion


        #region private methodes
        /**\brief The method will check if the Login is correct
         *\param email the mail of the user  
         *\param givenpassword   the password in clear text
         *\return will retrun a bool
         */
        private bool IsLoginCorrect(string mail, string givenPassword)
        {
            bool result = false;
            List<List<object>> queryResut;
            string query = "SELECT userHashPsw From users where userEmailAddress ='" + mail.ToLower() +"';";

            BdConnector bdConnector = new BdConnector();

            string givenPasswordHashed = GetHashString(givenPassword);
            

           queryResut = bdConnector.SelectQuery(query);

            if (queryResut.Count == 1)
            {
                string actualPasswordHashed = (string)queryResut[0][0];

                
                if(actualPasswordHashed == givenPasswordHashed)
                {
                    result = true;
                }
            }
            return result;
        }

        /**\brief this method will check if an account already exists for an email
         *\param email the mail of the user  
         *\param givenpassword   the password in clear text
         *\return bool
         */
        private bool IsAccountExisting(string email)
        {
            bool result = false;
            List<List<object>> queryResut;
            string query = "SELECT UserEmailAddress From users where userEmailAddress ='" + email.ToLower()+"';";

            BdConnector bdConnector = new BdConnector();
            queryResut = bdConnector.SelectQuery(query);

           

            if (queryResut.Count >= 1)
            {
                result = true;
            }

                return result;
        }
       

        static byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = SHA256.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        static string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }
        #endregion
    }
    
    /**\brief This class is used to throw an error when the login failed or the account already exists at the register
     * 
     * 
     */
    public class InvalidCreditentialsException : Exception
    {
        /**\brief just Exception() with another name
         */
        public InvalidCreditentialsException() { }
        /**\brief just Exception() with another name
         */
        public InvalidCreditentialsException(string message) { }
    }

    

}
