﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
//using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;




namespace TSAM.Model
{

    /**\brief this class create an object from a json, it's the user configuration of our program 
     *\details many of the privates attributes have a prefix "app_" or "bd_" to be clear. after those prefix the names of the variables are in camelCase. "app_" is for the parameters of the HMI. "bd_" is for the parameters of the conection with the database.
     */

    public class Config
    {

        #region private attributes
        
        private int app_heigh;
        private int app_width;
        private string app_language;
        private string bd_server;
        private string bd_database;
        private string bd_uid;
        private string bd_password;
        private string filePath = @"" + Environment.GetEnvironmentVariable("appdata") + "\\LoginProject\\userPreferances.json";
        
        
        #endregion

        #region constructor
        /**\brief the constructor
         * 
         */
        public Config()
        {
            
            

        }
        #endregion

        #region accessors
       public int app_Heigh
        {
            get { return app_heigh; }
            set { app_heigh = value; }
        }

        public int app_Width
        {
            get { return app_width; }
            set { app_width = value; }

        }

        public string app_Language
        {
            get { return app_language; }
            set { app_language = value; }
        }

        public string bd_Server
        {
            get { return bd_server; }
            set { bd_server = value; }
        }

        public string bd_Database
        {
            get { return bd_database; }
            set { bd_database = value; }
        }

        public string bd_Uid
        {
            get { return bd_uid; }
            set { bd_uid = value; }
        }

        public string bd_Password
        {
            get { return bd_password; }
            set { bd_password = value; }
        }

        #endregion

        #region public methodes
        /**\brief this method save the object in a json, it create the directory and the file if it doesn't exist
         * 
         */
        public void Save()
        {
            
            string Json = JsonConvert.SerializeObject(this);

            int lastBS = filePath.LastIndexOf("\\");

            string dirPath = filePath.Substring(0, lastBS);

            if (!File.Exists(dirPath))
            {
                Directory.CreateDirectory(dirPath);
            }

            File.WriteAllText(filePath, Json);
            
        }

        /**\brief this method will load the data from a json into the object
         * 
         */
        public void Load() { 

            string jsonString;
            if (File.Exists(filePath)) {
                StreamReader file = File.OpenText(filePath);
                jsonString = file.ReadToEnd();
                file.Close();

                Config tempConfig = new Config();

                tempConfig = JsonConvert.DeserializeObject<Config>(jsonString);

                this.app_heigh = tempConfig.app_Heigh;
                this.app_width = tempConfig.app_Width;
                this.app_language = tempConfig.app_Language;
                this.bd_server = tempConfig.bd_Server;
                this.bd_database = tempConfig.bd_Database;
                this.bd_uid = tempConfig.bd_Uid;
                this.bd_password = tempConfig.bd_Password;


            }
            else
            {
                DefaultConfig();
                
            }

            
        }



        #endregion

        #region private methodes
        /*\brief This method is used if when we load the file doesn't exist, it set default parameters
         *
         */
        private void DefaultConfig()
        {
            app_heigh = 390;
            app_width = 490;
            app_language = "FR";
            bd_server = "127.0.0.1";
            bd_database = "TSAM";
            bd_uid = "user";
            bd_password = "Pa$$w0rd";
        }
        
         

         #endregion

    }
}
