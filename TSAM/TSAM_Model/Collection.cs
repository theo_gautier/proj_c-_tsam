﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TSAM.Model
{
    /**
     * \brief collection is a class who work like a library. it contains contents and is used to load every contents and load specific contents with Load and Search methods
     */

    public class Collection
    {
        #region private attributes
        private List<Content> contents;
        #endregion

        #region constructor
        /**
         * \brief just the constructor, nothing to say
         */
        
        public Collection()
        {
            contents = new List<Content>();

        }
        #endregion

        #region accessors
        public List<Content> Contents
        {
            get { return contents; }
        }
        #endregion

        #region public methodes

        /** \brief the search mothod. this method search a string in every content in the database.
         * 
         * \param argument  the string searched
         * \return return a list of contents
         */

        
        public List<Content> Search(string argument)
        {

            argument = removeCriticalChar(argument);

            string query = "Select contents.* from contents WHERE contents.Name LIKE '%"+argument+ "%'  ORDER BY name;";
            BdConnector bdConnector = new BdConnector();
            List<List<object>> queryResult = bdConnector.SelectQuery(query);
            CollectionBuilder(queryResult);


            return contents;
        }


        /** \brief the load method. it load every content in the database.
         * \return return a list of contents
         */
        
        public List<Content> Load()
        {
            

            string query = "Select * from contents ORDER BY name;";
            BdConnector bdConnector = new BdConnector();
            List<List<object>> queryResult = bdConnector.SelectQuery(query);
            CollectionBuilder(queryResult);
            
            return contents;
        }

        private void CollectionBuilder(List<List<object>> data)
        {
            foreach (List<object> obj in data)
            {
                Content content = new Content();
                content.Id = Convert.ToInt32(obj[0].ToString());
                content.Name = obj[1].ToString();
                content.Type = obj[2].ToString();
                content.NbEpisodes = Convert.ToInt32(obj[3]);
                content.NbSaisons = Convert.ToInt32(obj[4]);
                content.Producer = obj[5].ToString();
                content.IsAnime = Convert.ToBoolean(obj[6]);
                content.Synopsis = obj[7].ToString();
                content.ImageUrl = obj[8].ToString();
                contents.Add(content);




            }


        }

        /**\brief purge the list of contents
         * 
         */
        public void PurgeList()
        {
            contents = null;
            contents = new List<Content>();
        }
        #endregion
        #region private methods
        private string removeCriticalChar(string str)
        {
            string[] chrList = { "'", "\"", "\\", ";", "\"" };

            foreach (string chr in chrList)
            {
                str = str.Replace(chr, " ");
            }

            return str;
        }
        #endregion
    }

}
