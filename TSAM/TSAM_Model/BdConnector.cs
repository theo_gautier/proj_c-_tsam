﻿using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data.Common;

namespace TSAM.Model
{
    /**\brief this class is used as interface with the database
     * 
     */
    
    class BdConnector
    {
        #region private attributes
        private MySqlConnection connection;
        #endregion

        #region constructor
        /**\brief the constructor
         * 
         */
        public BdConnector()
        {
            InitConnection();
        }
        #endregion

        #region accessors
        #endregion

        #region public methodes
        /**
         *\brief Open connection to the database
         */

        
        public void OpenConnection()
        {
            try
            {
                connection.Open();
            }
            catch(Exception ex)
            {
                throw new Exception();
            }
        }

        /**
         *\brief Close connection to the database
         */
        public void CloseConnection()
        {
            connection.Dispose();
        }

        /**\brief is used for select querys
         * 
         */
        public List<List<object>> SelectQuery(string query)
        {
            List<List<object>> list = null;
            MySqlCommand cmd = connection.CreateCommand();
            cmd.CommandText = query;
            MySqlDataReader dataReader;
            
            try
            {
                connection.Open();
                dataReader = cmd.ExecuteReader();
            }catch(Exception ex)
            {
                throw new Exception();
            }

            
            //we go through the result of the select, we might get only one response. 
            //Despite this, we use a while
            list = new List<List<object>>();

            int i = 0;
            while (dataReader.Read())
            {
                list.Add(new List<object>());
                for(int j = 0; j < dataReader.FieldCount; j++)
                {
                    list[i].Add(dataReader.GetValue(j));
                }
                i++;    

            }
            dataReader.Close();
            connection.Close();
            return list;
            
            
        }

        /**\brief is used for insert querys
         * 
         */
        public void InsertQuery(string query)
        {
            // Create a SQL command
            MySqlCommand cmd = connection.CreateCommand();

            // SQL request
            cmd.CommandText = query;
            try
            {
                connection.Open();
                cmd.ExecuteNonQuery();
               
            }
            catch (Exception ex)
            {
                throw new Exception();
            }

            connection.Close();
        }
        #endregion

        #region private methodes

        /**
         *\brief Initialize the connection to the database
         */
        private void InitConnection()
        {
            Config config = new Config();
            config.Load();

            // Creation of the connection string : where, who
            // Avoid user id and pwd hardcoded
            string connectionString = "SERVER="+config.bd_Server+ "; DATABASE=" + config.bd_Database + "; UID=" + config.bd_Uid + "; PASSWORD=" + config.bd_Password ;
            connection = new MySqlConnection(connectionString);
        }
        #endregion
    }
}
