﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace TSAM.Model
{

    /**\brief this class represent a serie or anime and store it's data
     * 
     * 
     */
    public class Content
    {
        #region private attributes
        private int id;
        private string name;
        private string type;
        private int nbEpisodes;
        private int nbSaisons;
        private string producer;
        private string description;
        private bool isAnime;
        private string synopsis;
        private string imageUrl;
        
        #endregion

        #region constructor
        /**\brief this is the constructor
         * 
         */
        public Content()
        {

        }


        #endregion

        #region accessors
        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Type { get => type; set => type = value; }
        public int NbEpisodes { get => nbEpisodes; set => nbEpisodes = value; }
        public int NbSaisons { get => nbSaisons; set => nbSaisons = value; }
        public string Producer { get => producer; set => producer = value; }
        public bool IsAnime { get => isAnime; set => isAnime = value; }
        public string Synopsis { get => synopsis; set => synopsis = value; }
        public string ImageUrl { get => imageUrl; set => imageUrl = value; }


        #endregion

        #region public methodes
        /**\brief The toString retrun the name
         * 
         *\return the name of the content
         */
        public override string ToString()
        {
            return name;
        }

        /**\brief this method put the content in the database
         * 
         */

        public void Create(string name, string type, int nbEpisodes, int nbSaisons, string producer, bool isAnime, string synopsis,string imageUrl)
        {
            bool flag = false;
            
            name = removeCriticalChar(name);
            type = removeCriticalChar(type);
            producer = removeCriticalChar(producer);
            synopsis = removeCriticalChar(synopsis);
            imageUrl = removeCriticalChar(imageUrl);

            string errorList = "";

            if (string.IsNullOrEmpty(name))
            {
                errorList += "name";
                flag = true;
            }

            if (string.IsNullOrEmpty(type))
            {
                errorList += "errorintype";
                flag = true;
            }

            if (string.IsNullOrEmpty(producer))
            {
                errorList += "producer";
                flag = true;
            }

            if (string.IsNullOrEmpty(synopsis))
            {
                errorList += "synopsis";
                flag = true;
            }

            Uri uriResult;
            bool urlCheckResult = Uri.TryCreate(imageUrl, UriKind.Absolute, out uriResult)
                && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);

            if (!urlCheckResult)
            {
                errorList += "notUrl";
                flag = true;
            }

            if (flag)
            {
                throw new Exception(errorList);
            }


            string query = "INSERT INTO `tsam`.`contents` (`Name`, `Type`, `NbEpisodes`, `NbSaisons`, `Producer`, `IsAnime`, `Synopsis`, `ImageUrl`) VALUES ('" + name + "', '" + type + "', '" + nbEpisodes + "', '" + nbSaisons + "', '" + producer + "', " + Convert.ToInt16(isAnime) + ", '" + synopsis + "', '"+ imageUrl + "')";
            BdConnector bdConnector = new BdConnector();
            bdConnector.InsertQuery(query);

        }

        #endregion

        #region private method
        private string removeCriticalChar(string str)
        {
            string[] chrList = { "'", "\"","\\",";","\""};

            foreach(string chr in chrList)
            {
                str = str.Replace(chr, " ");
            }

            return str;
        }
        #endregion
    }
}
