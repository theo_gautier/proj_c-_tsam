﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TSAM.Model;

namespace TSAM.IHM
{
    public partial class frm_LoggedToggleContent : Form
    {
        User connectedUser;
        Content content;


        public frm_LoggedToggleContent(User connectedUser, Content content)
        {
            this.connectedUser = connectedUser;
            this.content = content;
            //change the lbl_EMAIL.text to contain the user email.
            InitializeComponent();
            lbl_EMAIL.Text = connectedUser.Mail;
        }





        /// <summary>
        /// when frm_Logged.localisation is modified then the actual localisation is saved on the config file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frm_Logged_LocationChanged(object sender, EventArgs e)
        {
            Config configloader = new Config();
            configloader.Load();
            configloader.app_Heigh = this.Location.Y;
            configloader.app_Width = this.Location.X;
            configloader.Save();
        }

        /// <summary>
        /// Load the configuration file to set the location of the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frm_Logged_Load(object sender, EventArgs e)
        {
            Config startConfig = new Config();
            startConfig.Load();
            this.Location = new Point(startConfig.app_Width, startConfig.app_Heigh);

            string nbrEpisode = Convert.ToString(content.NbEpisodes);
            string nbrSeason = Convert.ToString(content.NbSaisons);

            lbl_ContentName.Text = content.Name;
            txt_ContentType.Text = content.Type;
            txt_ContentnbrEpisode.Text = nbrEpisode;
            txt_ContentNbrSeason.Text = nbrSeason;
            txt_ContentProducer.Text = content.Producer;
            Rtxt_ContentSynopsis.Text = content.Synopsis;
            try
            {
                pic_ContentPicture.Load(content.ImageUrl);
            }            
            catch(System.Net.WebException)
            {

            }
        }

   

     

        /// <summary>
        /// close the form when another form is being loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void f_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Close();
        }


        /// <summary>
        /// minimize the current form when the minimize button is clicked on
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pic_Minimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        /// <summary>
        /// close the current form when the closing button is clicked on
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pic_Close_Click(object sender, EventArgs e)
        {
            this.Close();       }

        private void pic_Search_Click(object sender, EventArgs e)
        {
            frm_LoggedSearch f = new frm_LoggedSearch(connectedUser);
            f.FormClosing += new FormClosingEventHandler(f_FormClosing);
            this.Hide();
            f.Show();
        }

        private void btn_Logout_Click(object sender, EventArgs e)
        {
            frm_Login f = new frm_Login();
            f.FormClosing += new FormClosingEventHandler(f_FormClosing);
            this.Hide();
            f.Show();
        }

        private void pic_ContentPicture_Click(object sender, EventArgs e)
        {

        }
    }

      
}
