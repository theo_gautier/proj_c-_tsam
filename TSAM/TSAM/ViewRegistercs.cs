﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TSAM.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;


namespace TSAM.IHM
{
    public partial class frm_Register : Form
    {
        private int indexLastChange;
        private StringBuilder password = new StringBuilder();
        private StringBuilder verfPassword = new StringBuilder();
        private TimerCallback timerCallback;
        private System.Threading.Timer timer;

        public frm_Register()
        {
            InitializeComponent();
            timerCallback = new TimerCallback(Do);
        }


        /// <summary>
        ///  Load the configuration file to set the location of the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnToLogin_Click(object sender, EventArgs e)
        {
           
            frm_Login f = new frm_Login();
            f.FormClosing += new FormClosingEventHandler(f_FormClosing);
            this.Hide();
            f.Show();
        }

        /// <summary>
        ///function designed to close the form after next form to open is loaded 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void f_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// when btnValidateINscription is pressed then:
        /// check if the email format is validate
        /// check if both password entry are the same
        /// check if the password is at least more than 8 caracter
        /// then send the register info the the model
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnValidateInscription_Click(object sender, EventArgs e)
        {
            //check the email entry format
            RegexUtilities mzjnthfd = new RegexUtilities();
            if (mzjnthfd.IsValidEmail(txtEntryEmail.Text))
            {
                
                //check if both password.text are the same
                if (verfPassword.ToString() == password.ToString())
                {
                    //check if password.text is more than 8 caracter
                    if(txtEntryPSW.Text.Length >= 8)
                    {
                        bool verification = true;

                        //send the register info to the model
                        try
                        {
                            User ConnectedUser = new User();
                            string inputPassword;
                            inputPassword = password.ToString();

                            ConnectedUser.Register(txtEntryEmail.Text, inputPassword);
                            string test = "";
                        }
                        //catch if a specifig error is comming from the model
                        catch (InvalidCreditentialsException error)
                        {

                            //if invalidCreditentialsException is catched then :
                            lbl_error.Text = "L'inscription à échouée.";
                            lbl_error.Visible = true;
                            verification = false;


                        }
                        //catch if an error come from the model
                        //this prevent the application from crash if the connection to the db is lost
                        catch (Exception noConnected)
                        {

                            //if any error (other than InvalidCreditentialsException ) is catched then :
                            lbl_error.Text = "Nos services sont actuellement en maintenance";
                          lbl_error.Visible = true;
                           verification = false;



                        }
                        //if all the test of catch and if are passed then we can open registerValidation form
                        if (verification == true)
                        {
                            lbl_error.Visible = false;
                            //we use txt_EntryEmail.text because we need it on the registerValidation form
                            frm_ValidateRegister f = new frm_ValidateRegister(txtEntryEmail.Text);
                            f.FormClosing += new FormClosingEventHandler(f_FormClosing);
                            this.Hide();
                            f.Show();


                        }



                     

                    }
                    //if the password is shorter than 8 caracter then:
                    else
                    {
                        lbl_error.Visible = true;
                        lbl_error.Text = "Le mot de passe doit faire au minimum 8 caractères";


                    }

                  
                }
                //if both password entry aren't the same then;
                else
                {
                    lbl_error.Visible = true;
                    lbl_error.Text = "Les deux mots de passe ne sont pas identiques";
                }
            }
            //if the email format isn't validated then :
            else
            {
                lbl_error.Visible = true;
                lbl_error.Text = "Veuillez entrer une adresse email valable";


            }
               


            

            
        }

        private void txtEntryEmail_TextChanged(object sender, EventArgs e)
        {

        }

        

        private void lbl_errorPassword_Click(object sender, EventArgs e)
        {

        }


        /// <summary>
        ///  when frm_register.localisation is modified then the actual localisation is saved on the config file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frm_Register_LocationChanged(object sender, EventArgs e)
        {
            Config configloader = new Config();
            configloader.Load();
            configloader.app_Heigh = this.Location.Y;
            configloader.app_Width = this.Location.X;
            configloader.Save();
        }

        /// <summary>
        /// Load the configuration file to set the location of the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frm_Register_Load(object sender, EventArgs e)
        {
            Config startConfig = new Config();
            startConfig.Load();
            this.Location = new Point(startConfig.app_Width, startConfig.app_Heigh);
        }

        private void txtEntryPSW_TextChanged(object sender, EventArgs e)
        {
            StringBuilder input = new StringBuilder(txtEntryPSW.Text); //set the input of the textbox to a string builder
            var cursorIndex = txtEntryPSW.SelectionStart;   // Get the starting position of the cursor

            for (int i = 0; i < txtEntryPSW.Text.Count(); i++)   //Go through the textbox's characters
            {
                if (input[i] != '●')
                {
                    if (i >= password.Length)
                    {
                        password.Append(input[i]);
                    }
                    else
                    {
                        if (input.Length > password.Length)
                        {
                            password.Insert(i, input[i]); //insert if a letter has been typed in the middle of the text into the clear stringBuilder

                        }
                        else
                        {
                            password[i] = txtEntryPSW.Text.ElementAt(i); //replace the letter

                        }
                    }

                    if (indexLastChange == i && input.Length > 0) //replace the clear character in the textbox
                    {
                        input[i] = '●';
                    }

                    indexLastChange = i; //Update the last index of a character that has changed
                }
            }

            int dif = password.Length - input.Length;
            if (dif > 0)
            {
                password.Remove(cursorIndex, dif); //remove a certain number of numbers in the clear password if have removed one or more characters

            }


            txtEntryPSW.Text = input.ToString(); //update the textbox
            Console.WriteLine("password " + password); //show the password in console


            if (timer == null)
            {

                timer = new System.Threading.Timer(timerCallback, null, 1000, 0); //create  a new timer if the old one has been destroyed
            }
            else
            {


                timer.Change(1000, 0);  //reset the timer to 1000ms


            }
            int num = this.txtEntryPSW.Text.Count();
            if (num > 1)
            {
                StringBuilder s = new StringBuilder(this.txtEntryPSW.Text);
                s[num - 2] = '●'; //replace the before to last character to a star
                this.txtEntryPSW.Text = s.ToString();
                this.txtEntryPSW.SelectionStart = num;
            }

            this.txtEntryPSW.SelectionStart = (cursorIndex > 0) ? cursorIndex : 0;   //third operator , test if the cursor is negative , it will go back to 0

            Console.WriteLine("Last index change :" + (indexLastChange)); //show the index in the console

        }
        private void Do(object state)
        {


            int num = this.txtEntryPSW.Text.Count();
            if (num > 0)
            {
                StringBuilder s = new StringBuilder(this.txtEntryPSW.Text);

                s[num - 1] = '●';                                   //hide the last character after one second
                this.Invoke(new Action(() =>
                {
                    var cursorIndex = txtEntryPSW.SelectionStart;   //get the cursor index
                    this.txtEntryPSW.Text = s.ToString();           // update the textbox
                    this.txtEntryPSW.SelectionStart = cursorIndex;  //reset the cursor to the current cursor
                    timer.Dispose();                                //destroy the timer
                    timer = null;
                }));
            }
        }

        private void txtConfirmPSW_TextChanged(object sender, EventArgs e)
        {

            StringBuilder input = new StringBuilder(txtConfirmPSW.Text); //set the input of the textbox to a string builder
            var cursorIndex = txtConfirmPSW.SelectionStart;   // Get the starting position of the cursor

            for (int i = 0; i < txtConfirmPSW.Text.Count(); i++)   //Go through the textbox's characters
            {
                if (input[i] != '●')
                {
                    if (i >= verfPassword.Length)
                    {
                        verfPassword.Append(input[i]);
                    }
                    else
                    {
                        if (input.Length > verfPassword.Length)
                        {
                            verfPassword.Insert(i, input[i]); //insert if a letter has been typed in the middle of the text into the clear stringBuilder

                        }
                        else
                        {
                            verfPassword[i] = txtConfirmPSW.Text.ElementAt(i); //replace the letter

                        }
                    }

                    if (indexLastChange == i && input.Length > 0) //replace the clear character in the textbox
                    {
                        input[i] = '●';
                    }

                    indexLastChange = i; //Update the last index of a character that has changed
                }
            }

            int dif = verfPassword.Length - input.Length;
            if (dif > 0)
            {
                verfPassword.Remove(cursorIndex, dif); //remove a certain number of numbers in the clear password if have removed one or more characters

            }


            txtConfirmPSW.Text = input.ToString(); //update the textbox
            Console.WriteLine("password " + verfPassword); //show the password in console


            if (timer == null)
            {

                timer = new System.Threading.Timer(timerCallback, null, 1000, 0); //create  a new timer if the old one has been destroyed
            }
            else
            {


                timer.Change(1000, 0);  //reset the timer to 1000ms


            }
            int num = this.txtConfirmPSW.Text.Count();
            if (num > 1)
            {
                StringBuilder s = new StringBuilder(this.txtConfirmPSW.Text);
                s[num - 2] = '●'; //replace the before to last character to a star
                this.txtConfirmPSW.Text = s.ToString();
                this.txtConfirmPSW.SelectionStart = num;
            }

            this.txtConfirmPSW.SelectionStart = (cursorIndex > 0) ? cursorIndex : 0;   //third operator , test if the cursor is negative , it will go back to 0

            Console.WriteLine("Last index change :" + (indexLastChange)); //show the index in the console

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
    }
}
