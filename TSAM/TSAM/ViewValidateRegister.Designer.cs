﻿namespace TSAM.IHM
{
    partial class frm_ValidateRegister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_ValidateRegister));
            this.lblConfirmedregistation = new System.Windows.Forms.Label();
            this.lblCongratulation = new System.Windows.Forms.Label();
            this.btnToLogin = new System.Windows.Forms.Button();
            this.pic_TSAMLogo = new System.Windows.Forms.PictureBox();
            this.pic_Close = new System.Windows.Forms.PictureBox();
            this.pic_Minimize = new System.Windows.Forms.PictureBox();
            this.pic_Cake = new System.Windows.Forms.PictureBox();
            this.lbl_MessageCongrat = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pic_TSAMLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Close)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Minimize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Cake)).BeginInit();
            this.SuspendLayout();
            // 
            // lblConfirmedregistation
            // 
            this.lblConfirmedregistation.AutoSize = true;
            this.lblConfirmedregistation.BackColor = System.Drawing.Color.Transparent;
            this.lblConfirmedregistation.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConfirmedregistation.Location = new System.Drawing.Point(65, 115);
            this.lblConfirmedregistation.Name = "lblConfirmedregistation";
            this.lblConfirmedregistation.Size = new System.Drawing.Size(346, 39);
            this.lblConfirmedregistation.TabIndex = 0;
            this.lblConfirmedregistation.Text = "Inscription Confirmée";
            // 
            // lblCongratulation
            // 
            this.lblCongratulation.AutoSize = true;
            this.lblCongratulation.BackColor = System.Drawing.Color.Transparent;
            this.lblCongratulation.Font = new System.Drawing.Font("Rockwell", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCongratulation.Location = new System.Drawing.Point(66, 318);
            this.lblCongratulation.Name = "lblCongratulation";
            this.lblCongratulation.Size = new System.Drawing.Size(63, 23);
            this.lblCongratulation.TabIndex = 1;
            this.lblCongratulation.Text = "email";
            this.lblCongratulation.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnToLogin
            // 
            this.btnToLogin.Font = new System.Drawing.Font("Rockwell", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnToLogin.Location = new System.Drawing.Point(166, 393);
            this.btnToLogin.Name = "btnToLogin";
            this.btnToLogin.Size = new System.Drawing.Size(139, 56);
            this.btnToLogin.TabIndex = 2;
            this.btnToLogin.Text = "Connectez-vous !";
            this.btnToLogin.UseVisualStyleBackColor = true;
            this.btnToLogin.Click += new System.EventHandler(this.btnToLogin_Click);
            // 
            // pic_TSAMLogo
            // 
            this.pic_TSAMLogo.BackColor = System.Drawing.Color.Transparent;
            this.pic_TSAMLogo.Image = ((System.Drawing.Image)(resources.GetObject("pic_TSAMLogo.Image")));
            this.pic_TSAMLogo.Location = new System.Drawing.Point(12, 12);
            this.pic_TSAMLogo.Name = "pic_TSAMLogo";
            this.pic_TSAMLogo.Size = new System.Drawing.Size(161, 74);
            this.pic_TSAMLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_TSAMLogo.TabIndex = 9;
            this.pic_TSAMLogo.TabStop = false;
            // 
            // pic_Close
            // 
            this.pic_Close.BackColor = System.Drawing.Color.Transparent;
            this.pic_Close.Image = ((System.Drawing.Image)(resources.GetObject("pic_Close.Image")));
            this.pic_Close.Location = new System.Drawing.Point(440, 2);
            this.pic_Close.Name = "pic_Close";
            this.pic_Close.Size = new System.Drawing.Size(42, 33);
            this.pic_Close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_Close.TabIndex = 10;
            this.pic_Close.TabStop = false;
            this.pic_Close.Click += new System.EventHandler(this.pic_Close_Click);
            // 
            // pic_Minimize
            // 
            this.pic_Minimize.BackColor = System.Drawing.Color.Transparent;
            this.pic_Minimize.Image = ((System.Drawing.Image)(resources.GetObject("pic_Minimize.Image")));
            this.pic_Minimize.Location = new System.Drawing.Point(397, 2);
            this.pic_Minimize.Name = "pic_Minimize";
            this.pic_Minimize.Size = new System.Drawing.Size(37, 33);
            this.pic_Minimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_Minimize.TabIndex = 11;
            this.pic_Minimize.TabStop = false;
            this.pic_Minimize.Click += new System.EventHandler(this.pic_Minimize_Click);
            // 
            // pic_Cake
            // 
            this.pic_Cake.BackColor = System.Drawing.Color.Transparent;
            this.pic_Cake.Image = ((System.Drawing.Image)(resources.GetObject("pic_Cake.Image")));
            this.pic_Cake.Location = new System.Drawing.Point(106, 108);
            this.pic_Cake.Name = "pic_Cake";
            this.pic_Cake.Size = new System.Drawing.Size(276, 233);
            this.pic_Cake.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_Cake.TabIndex = 12;
            this.pic_Cake.TabStop = false;
            // 
            // lbl_MessageCongrat
            // 
            this.lbl_MessageCongrat.AutoSize = true;
            this.lbl_MessageCongrat.BackColor = System.Drawing.Color.Transparent;
            this.lbl_MessageCongrat.Font = new System.Drawing.Font("Rockwell", 15.75F);
            this.lbl_MessageCongrat.Location = new System.Drawing.Point(67, 358);
            this.lbl_MessageCongrat.Name = "lbl_MessageCongrat";
            this.lbl_MessageCongrat.Size = new System.Drawing.Size(232, 23);
            this.lbl_MessageCongrat.TabIndex = 13;
            this.lbl_MessageCongrat.Text = "vous êtes bien inscrit-e";
            // 
            // frm_ValidateRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(484, 461);
            this.Controls.Add(this.lbl_MessageCongrat);
            this.Controls.Add(this.pic_Minimize);
            this.Controls.Add(this.pic_Close);
            this.Controls.Add(this.pic_TSAMLogo);
            this.Controls.Add(this.btnToLogin);
            this.Controls.Add(this.lblCongratulation);
            this.Controls.Add(this.lblConfirmedregistation);
            this.Controls.Add(this.pic_Cake);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "frm_ValidateRegister";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "ViewValidateRegister";
            this.Load += new System.EventHandler(this.frm_ValidateRegister_Load);
            this.LocationChanged += new System.EventHandler(this.frm_ValidateRegister_LocationChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pic_TSAMLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Close)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Minimize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Cake)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblConfirmedregistation;
        private System.Windows.Forms.Label lblCongratulation;
        private System.Windows.Forms.Button btnToLogin;
        private System.Windows.Forms.PictureBox pic_TSAMLogo;
        private System.Windows.Forms.PictureBox pic_Close;
        private System.Windows.Forms.PictureBox pic_Minimize;
        private System.Windows.Forms.PictureBox pic_Cake;
        private System.Windows.Forms.Label lbl_MessageCongrat;
    }
}