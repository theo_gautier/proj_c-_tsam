﻿namespace TSAM.IHM
{
    partial class frm_Register
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Register));
            this.lblInscription = new System.Windows.Forms.Label();
            this.lblEntryEmail = new System.Windows.Forms.Label();
            this.lblEntryPSW = new System.Windows.Forms.Label();
            this.txtEntryEmail = new System.Windows.Forms.TextBox();
            this.txtEntryPSW = new System.Windows.Forms.TextBox();
            this.lblConfirmPSW = new System.Windows.Forms.Label();
            this.txtConfirmPSW = new System.Windows.Forms.TextBox();
            this.lbl_error = new System.Windows.Forms.Label();
            this.btnToLogin = new System.Windows.Forms.Button();
            this.btnValidateInscription = new System.Windows.Forms.Button();
            this.pic_TSAMLogo = new System.Windows.Forms.PictureBox();
            this.pic_Minimize = new System.Windows.Forms.PictureBox();
            this.pic_Close = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pic_TSAMLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Minimize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Close)).BeginInit();
            this.SuspendLayout();
            // 
            // lblInscription
            // 
            this.lblInscription.AutoSize = true;
            this.lblInscription.BackColor = System.Drawing.Color.Transparent;
            this.lblInscription.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInscription.Location = new System.Drawing.Point(142, 116);
            this.lblInscription.Name = "lblInscription";
            this.lblInscription.Size = new System.Drawing.Size(176, 39);
            this.lblInscription.TabIndex = 0;
            this.lblInscription.Text = "Inscription";
            // 
            // lblEntryEmail
            // 
            this.lblEntryEmail.AutoSize = true;
            this.lblEntryEmail.BackColor = System.Drawing.Color.Transparent;
            this.lblEntryEmail.Font = new System.Drawing.Font("Rockwell", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEntryEmail.Location = new System.Drawing.Point(146, 171);
            this.lblEntryEmail.Name = "lblEntryEmail";
            this.lblEntryEmail.Size = new System.Drawing.Size(112, 16);
            this.lblEntryEmail.TabIndex = 1;
            this.lblEntryEmail.Text = "Entrez votre email";
            // 
            // lblEntryPSW
            // 
            this.lblEntryPSW.AutoSize = true;
            this.lblEntryPSW.BackColor = System.Drawing.Color.Transparent;
            this.lblEntryPSW.Font = new System.Drawing.Font("Rockwell", 9.75F);
            this.lblEntryPSW.Location = new System.Drawing.Point(146, 231);
            this.lblEntryPSW.Name = "lblEntryPSW";
            this.lblEntryPSW.Size = new System.Drawing.Size(85, 16);
            this.lblEntryPSW.TabIndex = 2;
            this.lblEntryPSW.Text = "Mot de passe";
            // 
            // txtEntryEmail
            // 
            this.txtEntryEmail.Location = new System.Drawing.Point(149, 191);
            this.txtEntryEmail.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtEntryEmail.Name = "txtEntryEmail";
            this.txtEntryEmail.Size = new System.Drawing.Size(226, 23);
            this.txtEntryEmail.TabIndex = 1;
            this.txtEntryEmail.TextChanged += new System.EventHandler(this.txtEntryEmail_TextChanged);
            // 
            // txtEntryPSW
            // 
            this.txtEntryPSW.Location = new System.Drawing.Point(149, 251);
            this.txtEntryPSW.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtEntryPSW.Name = "txtEntryPSW";
            this.txtEntryPSW.ShortcutsEnabled = false;
            this.txtEntryPSW.Size = new System.Drawing.Size(226, 23);
            this.txtEntryPSW.TabIndex = 2;
            this.txtEntryPSW.TextChanged += new System.EventHandler(this.txtEntryPSW_TextChanged);
            // 
            // lblConfirmPSW
            // 
            this.lblConfirmPSW.AutoSize = true;
            this.lblConfirmPSW.BackColor = System.Drawing.Color.Transparent;
            this.lblConfirmPSW.Font = new System.Drawing.Font("Rockwell", 9.75F);
            this.lblConfirmPSW.Location = new System.Drawing.Point(148, 290);
            this.lblConfirmPSW.Name = "lblConfirmPSW";
            this.lblConfirmPSW.Size = new System.Drawing.Size(170, 16);
            this.lblConfirmPSW.TabIndex = 6;
            this.lblConfirmPSW.Text = "Mot de passe (confirmation)";
            // 
            // txtConfirmPSW
            // 
            this.txtConfirmPSW.Location = new System.Drawing.Point(149, 310);
            this.txtConfirmPSW.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtConfirmPSW.Name = "txtConfirmPSW";
            this.txtConfirmPSW.ShortcutsEnabled = false;
            this.txtConfirmPSW.Size = new System.Drawing.Size(226, 23);
            this.txtConfirmPSW.TabIndex = 3;
            this.txtConfirmPSW.TextChanged += new System.EventHandler(this.txtConfirmPSW_TextChanged);
            // 
            // lbl_error
            // 
            this.lbl_error.AutoSize = true;
            this.lbl_error.BackColor = System.Drawing.Color.Transparent;
            this.lbl_error.ForeColor = System.Drawing.Color.Maroon;
            this.lbl_error.Location = new System.Drawing.Point(148, 348);
            this.lbl_error.Name = "lbl_error";
            this.lbl_error.Size = new System.Drawing.Size(196, 16);
            this.lbl_error.TabIndex = 9;
            this.lbl_error.Text = "Entrer une adresse email valable";
            this.lbl_error.Visible = false;
            // 
            // btnToLogin
            // 
            this.btnToLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btnToLogin.Location = new System.Drawing.Point(288, 404);
            this.btnToLogin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnToLogin.Name = "btnToLogin";
            this.btnToLogin.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnToLogin.Size = new System.Drawing.Size(87, 28);
            this.btnToLogin.TabIndex = 5;
            this.btnToLogin.Text = "login";
            this.btnToLogin.UseVisualStyleBackColor = true;
            this.btnToLogin.Click += new System.EventHandler(this.btnToLogin_Click);
            // 
            // btnValidateInscription
            // 
            this.btnValidateInscription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btnValidateInscription.Location = new System.Drawing.Point(229, 368);
            this.btnValidateInscription.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnValidateInscription.Name = "btnValidateInscription";
            this.btnValidateInscription.Size = new System.Drawing.Size(146, 28);
            this.btnValidateInscription.TabIndex = 4;
            this.btnValidateInscription.Text = "Valider votre Inscription";
            this.btnValidateInscription.UseVisualStyleBackColor = true;
            this.btnValidateInscription.Click += new System.EventHandler(this.btnValidateInscription_Click);
            // 
            // pic_TSAMLogo
            // 
            this.pic_TSAMLogo.BackColor = System.Drawing.Color.Transparent;
            this.pic_TSAMLogo.Image = ((System.Drawing.Image)(resources.GetObject("pic_TSAMLogo.Image")));
            this.pic_TSAMLogo.Location = new System.Drawing.Point(14, 15);
            this.pic_TSAMLogo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_TSAMLogo.Name = "pic_TSAMLogo";
            this.pic_TSAMLogo.Size = new System.Drawing.Size(163, 70);
            this.pic_TSAMLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_TSAMLogo.TabIndex = 10;
            this.pic_TSAMLogo.TabStop = false;
            // 
            // pic_Minimize
            // 
            this.pic_Minimize.BackColor = System.Drawing.Color.Transparent;
            this.pic_Minimize.Image = ((System.Drawing.Image)(resources.GetObject("pic_Minimize.Image")));
            this.pic_Minimize.Location = new System.Drawing.Point(392, 1);
            this.pic_Minimize.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Minimize.Name = "pic_Minimize";
            this.pic_Minimize.Size = new System.Drawing.Size(40, 38);
            this.pic_Minimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_Minimize.TabIndex = 11;
            this.pic_Minimize.TabStop = false;
            this.pic_Minimize.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // pic_Close
            // 
            this.pic_Close.BackColor = System.Drawing.Color.Transparent;
            this.pic_Close.Image = ((System.Drawing.Image)(resources.GetObject("pic_Close.Image")));
            this.pic_Close.Location = new System.Drawing.Point(438, 1);
            this.pic_Close.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Close.Name = "pic_Close";
            this.pic_Close.Size = new System.Drawing.Size(47, 38);
            this.pic_Close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_Close.TabIndex = 12;
            this.pic_Close.TabStop = false;
            this.pic_Close.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // frm_Register
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(484, 461);
            this.ControlBox = false;
            this.Controls.Add(this.pic_Close);
            this.Controls.Add(this.pic_Minimize);
            this.Controls.Add(this.pic_TSAMLogo);
            this.Controls.Add(this.txtEntryEmail);
            this.Controls.Add(this.txtEntryPSW);
            this.Controls.Add(this.txtConfirmPSW);
            this.Controls.Add(this.btnValidateInscription);
            this.Controls.Add(this.btnToLogin);
            this.Controls.Add(this.lbl_error);
            this.Controls.Add(this.lblConfirmPSW);
            this.Controls.Add(this.lblEntryPSW);
            this.Controls.Add(this.lblEntryEmail);
            this.Controls.Add(this.lblInscription);
            this.Font = new System.Drawing.Font("Rockwell", 9.75F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_Register";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = " ";
            this.Load += new System.EventHandler(this.frm_Register_Load);
            this.LocationChanged += new System.EventHandler(this.frm_Register_LocationChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pic_TSAMLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Minimize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Close)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblInscription;
        private System.Windows.Forms.Label lblEntryEmail;
        private System.Windows.Forms.Label lblEntryPSW;
        private System.Windows.Forms.TextBox txtEntryEmail;
        private System.Windows.Forms.TextBox txtEntryPSW;
        private System.Windows.Forms.Label lblConfirmPSW;
        private System.Windows.Forms.TextBox txtConfirmPSW;
        private System.Windows.Forms.Label lbl_error;
        private System.Windows.Forms.Button btnToLogin;
        private System.Windows.Forms.Button btnValidateInscription;
        private System.Windows.Forms.PictureBox pic_TSAMLogo;
        private System.Windows.Forms.PictureBox pic_Minimize;
        private System.Windows.Forms.PictureBox pic_Close;
    }
}