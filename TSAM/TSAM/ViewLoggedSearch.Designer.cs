﻿namespace TSAM.IHM
{
    partial class frm_LoggedSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_LoggedSearch));
            this.lbl_UserEmail = new System.Windows.Forms.Label();
            this.lbl_EMAIL = new System.Windows.Forms.Label();
            this.pic_TSAMLogo = new System.Windows.Forms.PictureBox();
            this.pic_Search = new System.Windows.Forms.PictureBox();
            this.pic_News = new System.Windows.Forms.PictureBox();
            this.pic_MyFollow = new System.Windows.Forms.PictureBox();
            this.pic_Profil = new System.Windows.Forms.PictureBox();
            this.pic_Minimize = new System.Windows.Forms.PictureBox();
            this.pic_Close = new System.Windows.Forms.PictureBox();
            this.pic_CreateSeries = new System.Windows.Forms.PictureBox();
            this.lbl_SearchBox = new System.Windows.Forms.Label();
            this.txtBox_SearchContent = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lst_Collection = new System.Windows.Forms.ListBox();
            this.btn_Search = new System.Windows.Forms.Button();
            this.btn_ToggleContent = new System.Windows.Forms.Button();
            this.btn_Logout = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pic_TSAMLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_News)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_MyFollow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Profil)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Minimize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Close)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_CreateSeries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_UserEmail
            // 
            this.lbl_UserEmail.AutoSize = true;
            this.lbl_UserEmail.Location = new System.Drawing.Point(669, 9);
            this.lbl_UserEmail.Name = "lbl_UserEmail";
            this.lbl_UserEmail.Size = new System.Drawing.Size(64, 13);
            this.lbl_UserEmail.TabIndex = 0;
            this.lbl_UserEmail.Text = "\"user email\"";
            // 
            // lbl_EMAIL
            // 
            this.lbl_EMAIL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_EMAIL.BackColor = System.Drawing.Color.Transparent;
            this.lbl_EMAIL.Location = new System.Drawing.Point(668, 59);
            this.lbl_EMAIL.Name = "lbl_EMAIL";
            this.lbl_EMAIL.Size = new System.Drawing.Size(129, 13);
            this.lbl_EMAIL.TabIndex = 0;
            this.lbl_EMAIL.Text = "qwertzu";
            this.lbl_EMAIL.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pic_TSAMLogo
            // 
            this.pic_TSAMLogo.BackColor = System.Drawing.Color.Transparent;
            this.pic_TSAMLogo.Image = ((System.Drawing.Image)(resources.GetObject("pic_TSAMLogo.Image")));
            this.pic_TSAMLogo.Location = new System.Drawing.Point(12, 0);
            this.pic_TSAMLogo.Name = "pic_TSAMLogo";
            this.pic_TSAMLogo.Size = new System.Drawing.Size(204, 81);
            this.pic_TSAMLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_TSAMLogo.TabIndex = 1;
            this.pic_TSAMLogo.TabStop = false;
            // 
            // pic_Search
            // 
            this.pic_Search.BackColor = System.Drawing.Color.Transparent;
            this.pic_Search.Image = ((System.Drawing.Image)(resources.GetObject("pic_Search.Image")));
            this.pic_Search.InitialImage = null;
            this.pic_Search.Location = new System.Drawing.Point(254, 12);
            this.pic_Search.Name = "pic_Search";
            this.pic_Search.Size = new System.Drawing.Size(55, 60);
            this.pic_Search.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_Search.TabIndex = 8;
            this.pic_Search.TabStop = false;
            this.pic_Search.Click += new System.EventHandler(this.pic_Search_Click);
            // 
            // pic_News
            // 
            this.pic_News.BackColor = System.Drawing.Color.Transparent;
            this.pic_News.Image = ((System.Drawing.Image)(resources.GetObject("pic_News.Image")));
            this.pic_News.InitialImage = null;
            this.pic_News.Location = new System.Drawing.Point(460, 12);
            this.pic_News.Name = "pic_News";
            this.pic_News.Size = new System.Drawing.Size(55, 60);
            this.pic_News.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_News.TabIndex = 9;
            this.pic_News.TabStop = false;
            // 
            // pic_MyFollow
            // 
            this.pic_MyFollow.BackColor = System.Drawing.Color.Transparent;
            this.pic_MyFollow.Image = ((System.Drawing.Image)(resources.GetObject("pic_MyFollow.Image")));
            this.pic_MyFollow.InitialImage = null;
            this.pic_MyFollow.Location = new System.Drawing.Point(357, 12);
            this.pic_MyFollow.Name = "pic_MyFollow";
            this.pic_MyFollow.Size = new System.Drawing.Size(55, 60);
            this.pic_MyFollow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_MyFollow.TabIndex = 10;
            this.pic_MyFollow.TabStop = false;
            // 
            // pic_Profil
            // 
            this.pic_Profil.BackColor = System.Drawing.Color.Transparent;
            this.pic_Profil.Image = ((System.Drawing.Image)(resources.GetObject("pic_Profil.Image")));
            this.pic_Profil.InitialImage = null;
            this.pic_Profil.Location = new System.Drawing.Point(758, 12);
            this.pic_Profil.Name = "pic_Profil";
            this.pic_Profil.Size = new System.Drawing.Size(39, 36);
            this.pic_Profil.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_Profil.TabIndex = 11;
            this.pic_Profil.TabStop = false;
            // 
            // pic_Minimize
            // 
            this.pic_Minimize.BackColor = System.Drawing.Color.Transparent;
            this.pic_Minimize.Image = ((System.Drawing.Image)(resources.GetObject("pic_Minimize.Image")));
            this.pic_Minimize.Location = new System.Drawing.Point(803, 0);
            this.pic_Minimize.Name = "pic_Minimize";
            this.pic_Minimize.Size = new System.Drawing.Size(47, 36);
            this.pic_Minimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_Minimize.TabIndex = 13;
            this.pic_Minimize.TabStop = false;
            this.pic_Minimize.Click += new System.EventHandler(this.pic_Minimize_Click);
            // 
            // pic_Close
            // 
            this.pic_Close.BackColor = System.Drawing.Color.Transparent;
            this.pic_Close.Image = ((System.Drawing.Image)(resources.GetObject("pic_Close.Image")));
            this.pic_Close.Location = new System.Drawing.Point(856, 0);
            this.pic_Close.Name = "pic_Close";
            this.pic_Close.Size = new System.Drawing.Size(48, 36);
            this.pic_Close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_Close.TabIndex = 12;
            this.pic_Close.TabStop = false;
            this.pic_Close.Click += new System.EventHandler(this.pic_Close_Click);
            // 
            // pic_CreateSeries
            // 
            this.pic_CreateSeries.BackColor = System.Drawing.Color.Transparent;
            this.pic_CreateSeries.Image = ((System.Drawing.Image)(resources.GetObject("pic_CreateSeries.Image")));
            this.pic_CreateSeries.Location = new System.Drawing.Point(568, 12);
            this.pic_CreateSeries.Name = "pic_CreateSeries";
            this.pic_CreateSeries.Size = new System.Drawing.Size(64, 60);
            this.pic_CreateSeries.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_CreateSeries.TabIndex = 14;
            this.pic_CreateSeries.TabStop = false;
            // 
            // lbl_SearchBox
            // 
            this.lbl_SearchBox.AutoSize = true;
            this.lbl_SearchBox.BackColor = System.Drawing.Color.Transparent;
            this.lbl_SearchBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_SearchBox.Location = new System.Drawing.Point(7, 95);
            this.lbl_SearchBox.Name = "lbl_SearchBox";
            this.lbl_SearchBox.Size = new System.Drawing.Size(275, 29);
            this.lbl_SearchBox.TabIndex = 15;
            this.lbl_SearchBox.Text = "Rechercher Serie/animé";
            // 
            // txtBox_SearchContent
            // 
            this.txtBox_SearchContent.Location = new System.Drawing.Point(25, 161);
            this.txtBox_SearchContent.Name = "txtBox_SearchContent";
            this.txtBox_SearchContent.Size = new System.Drawing.Size(140, 20);
            this.txtBox_SearchContent.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 142);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 16);
            this.label1.TabIndex = 20;
            this.label1.Text = "Rechercher par Lettre :";
            // 
            // lst_Collection
            // 
            this.lst_Collection.FormattingEnabled = true;
            this.lst_Collection.Location = new System.Drawing.Point(121, 211);
            this.lst_Collection.Name = "lst_Collection";
            this.lst_Collection.Size = new System.Drawing.Size(308, 290);
            this.lst_Collection.TabIndex = 3;
            this.lst_Collection.SelectedIndexChanged += new System.EventHandler(this.lst_Collection_SelectedIndexChanged);
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(171, 159);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.Size = new System.Drawing.Size(75, 23);
            this.btn_Search.TabIndex = 2;
            this.btn_Search.Text = "Rechercher";
            this.btn_Search.UseVisualStyleBackColor = true;
            this.btn_Search.Click += new System.EventHandler(this.btn_Search_Click);
            // 
            // btn_ToggleContent
            // 
            this.btn_ToggleContent.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ToggleContent.Location = new System.Drawing.Point(397, 499);
            this.btn_ToggleContent.Name = "btn_ToggleContent";
            this.btn_ToggleContent.Size = new System.Drawing.Size(92, 31);
            this.btn_ToggleContent.TabIndex = 4;
            this.btn_ToggleContent.Text = "Afficher";
            this.btn_ToggleContent.UseVisualStyleBackColor = true;
            this.btn_ToggleContent.Click += new System.EventHandler(this.btn_ToggleContent_Click);
            // 
            // btn_Logout
            // 
            this.btn_Logout.BackColor = System.Drawing.SystemColors.Control;
            this.btn_Logout.Font = new System.Drawing.Font("Rockwell", 9.75F);
            this.btn_Logout.Location = new System.Drawing.Point(799, 51);
            this.btn_Logout.Name = "btn_Logout";
            this.btn_Logout.Size = new System.Drawing.Size(105, 30);
            this.btn_Logout.TabIndex = 28;
            this.btn_Logout.Text = "Deconnection";
            this.btn_Logout.UseVisualStyleBackColor = false;
            this.btn_Logout.Click += new System.EventHandler(this.btn_Logout_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(435, 211);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(260, 290);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 29;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // frm_LoggedSearch
            // 
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(903, 559);
            this.ControlBox = false;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btn_Logout);
            this.Controls.Add(this.btn_ToggleContent);
            this.Controls.Add(this.btn_Search);
            this.Controls.Add(this.lst_Collection);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtBox_SearchContent);
            this.Controls.Add(this.lbl_SearchBox);
            this.Controls.Add(this.pic_CreateSeries);
            this.Controls.Add(this.pic_Minimize);
            this.Controls.Add(this.pic_Close);
            this.Controls.Add(this.pic_News);
            this.Controls.Add(this.pic_MyFollow);
            this.Controls.Add(this.pic_Search);
            this.Controls.Add(this.pic_Profil);
            this.Controls.Add(this.pic_TSAMLogo);
            this.Controls.Add(this.lbl_EMAIL);
            this.MaximizeBox = false;
            this.Name = "frm_LoggedSearch";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = " ";
            this.Load += new System.EventHandler(this.frm_Logged_Load);
            this.LocationChanged += new System.EventHandler(this.frm_Logged_LocationChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pic_TSAMLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_News)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_MyFollow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Profil)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Minimize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Close)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_CreateSeries)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblConfirmedregistation;
        private System.Windows.Forms.Label lblCongratulation;
        private System.Windows.Forms.Button btnToLogin;
        private System.Windows.Forms.Label lbl_UserEmail;
        private System.Windows.Forms.Label lbl_EMAIL;
        private System.Windows.Forms.PictureBox pic_TSAMLogo;
        private System.Windows.Forms.PictureBox pic_Search;
        private System.Windows.Forms.PictureBox pic_News;
        private System.Windows.Forms.PictureBox pic_MyFollow;
        private System.Windows.Forms.PictureBox pic_Profil;
        private System.Windows.Forms.PictureBox pic_Minimize;
        private System.Windows.Forms.PictureBox pic_Close;
        private System.Windows.Forms.PictureBox pic_CreateSeries;
        private System.Windows.Forms.Label lbl_SearchBox;
        private System.Windows.Forms.TextBox txtBox_SearchContent;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lst_Collection;
        private System.Windows.Forms.Button btn_Search;
        private System.Windows.Forms.Button btn_ToggleContent;
        private System.Windows.Forms.Button btn_Logout;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}