﻿namespace TSAM
{
    partial class frm_AdminLogged
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_AdminLogged));
            this.pic_CreateSeries = new System.Windows.Forms.PictureBox();
            this.Pic_Minimize = new System.Windows.Forms.PictureBox();
            this.Pic_Close = new System.Windows.Forms.PictureBox();
            this.pic_Profil = new System.Windows.Forms.PictureBox();
            this.pic_TSAMLogo = new System.Windows.Forms.PictureBox();
            this.lbl_EMAIL = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btn_Logout = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pic_CreateSeries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pic_Minimize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pic_Close)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Profil)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_TSAMLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pic_CreateSeries
            // 
            this.pic_CreateSeries.BackColor = System.Drawing.Color.Transparent;
            this.pic_CreateSeries.Image = ((System.Drawing.Image)(resources.GetObject("pic_CreateSeries.Image")));
            this.pic_CreateSeries.Location = new System.Drawing.Point(251, 24);
            this.pic_CreateSeries.Name = "pic_CreateSeries";
            this.pic_CreateSeries.Size = new System.Drawing.Size(64, 60);
            this.pic_CreateSeries.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_CreateSeries.TabIndex = 31;
            this.pic_CreateSeries.TabStop = false;
            this.pic_CreateSeries.Click += new System.EventHandler(this.pic_CreateSeries_Click);
            // 
            // Pic_Minimize
            // 
            this.Pic_Minimize.BackColor = System.Drawing.Color.Transparent;
            this.Pic_Minimize.Image = ((System.Drawing.Image)(resources.GetObject("Pic_Minimize.Image")));
            this.Pic_Minimize.Location = new System.Drawing.Point(869, 12);
            this.Pic_Minimize.Name = "Pic_Minimize";
            this.Pic_Minimize.Size = new System.Drawing.Size(47, 36);
            this.Pic_Minimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Pic_Minimize.TabIndex = 30;
            this.Pic_Minimize.TabStop = false;
            this.Pic_Minimize.Click += new System.EventHandler(this.Pic_Minimize_Click);
            // 
            // Pic_Close
            // 
            this.Pic_Close.BackColor = System.Drawing.Color.Transparent;
            this.Pic_Close.Image = ((System.Drawing.Image)(resources.GetObject("Pic_Close.Image")));
            this.Pic_Close.Location = new System.Drawing.Point(922, 12);
            this.Pic_Close.Name = "Pic_Close";
            this.Pic_Close.Size = new System.Drawing.Size(48, 36);
            this.Pic_Close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Pic_Close.TabIndex = 29;
            this.Pic_Close.TabStop = false;
            this.Pic_Close.Click += new System.EventHandler(this.Pic_Close_Click);
            // 
            // pic_Profil
            // 
            this.pic_Profil.BackColor = System.Drawing.Color.Transparent;
            this.pic_Profil.Image = ((System.Drawing.Image)(resources.GetObject("pic_Profil.Image")));
            this.pic_Profil.InitialImage = null;
            this.pic_Profil.Location = new System.Drawing.Point(806, 23);
            this.pic_Profil.Name = "pic_Profil";
            this.pic_Profil.Size = new System.Drawing.Size(48, 44);
            this.pic_Profil.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_Profil.TabIndex = 28;
            this.pic_Profil.TabStop = false;
            // 
            // pic_TSAMLogo
            // 
            this.pic_TSAMLogo.BackColor = System.Drawing.Color.Transparent;
            this.pic_TSAMLogo.Image = ((System.Drawing.Image)(resources.GetObject("pic_TSAMLogo.Image")));
            this.pic_TSAMLogo.Location = new System.Drawing.Point(12, 2);
            this.pic_TSAMLogo.Name = "pic_TSAMLogo";
            this.pic_TSAMLogo.Size = new System.Drawing.Size(204, 81);
            this.pic_TSAMLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_TSAMLogo.TabIndex = 24;
            this.pic_TSAMLogo.TabStop = false;
            // 
            // lbl_EMAIL
            // 
            this.lbl_EMAIL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_EMAIL.BackColor = System.Drawing.Color.Transparent;
            this.lbl_EMAIL.Location = new System.Drawing.Point(725, 71);
            this.lbl_EMAIL.Name = "lbl_EMAIL";
            this.lbl_EMAIL.Size = new System.Drawing.Size(129, 13);
            this.lbl_EMAIL.TabIndex = 32;
            this.lbl_EMAIL.Text = "qwertzu";
            this.lbl_EMAIL.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::TSAM.Properties.Resources.Search;
            this.pictureBox1.Location = new System.Drawing.Point(332, 23);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(65, 61);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 33;
            this.pictureBox1.TabStop = false;
            // 
            // btn_Logout
            // 
            this.btn_Logout.BackColor = System.Drawing.SystemColors.Control;
            this.btn_Logout.Font = new System.Drawing.Font("Rockwell", 9.75F);
            this.btn_Logout.Location = new System.Drawing.Point(869, 62);
            this.btn_Logout.Name = "btn_Logout";
            this.btn_Logout.Size = new System.Drawing.Size(105, 30);
            this.btn_Logout.TabIndex = 34;
            this.btn_Logout.Text = "Deconnection";
            this.btn_Logout.UseVisualStyleBackColor = false;
            this.btn_Logout.Click += new System.EventHandler(this.btn_Logout_Click);
            // 
            // frm_AdminLogged
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(980, 537);
            this.ControlBox = false;
            this.Controls.Add(this.btn_Logout);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lbl_EMAIL);
            this.Controls.Add(this.pic_CreateSeries);
            this.Controls.Add(this.Pic_Minimize);
            this.Controls.Add(this.Pic_Close);
            this.Controls.Add(this.pic_Profil);
            this.Controls.Add(this.pic_TSAMLogo);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Name = "frm_AdminLogged";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = " ";
            this.Load += new System.EventHandler(this.frm_AdminLogged_Load);
            this.LocationChanged += new System.EventHandler(this.frm_AdminLogged_LocationChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pic_CreateSeries)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pic_Minimize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pic_Close)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Profil)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_TSAMLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pic_CreateSeries;
        private System.Windows.Forms.PictureBox Pic_Minimize;
        private System.Windows.Forms.PictureBox Pic_Close;
        private System.Windows.Forms.PictureBox pic_Profil;
        private System.Windows.Forms.PictureBox pic_TSAMLogo;
        private System.Windows.Forms.Label lbl_EMAIL;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btn_Logout;
    }
}