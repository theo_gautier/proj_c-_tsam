﻿namespace TSAM
{
    partial class frm_CreateSeries
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_CreateSeries));
            this.pic_CreateSeries = new System.Windows.Forms.PictureBox();
            this.Pic_Minimize = new System.Windows.Forms.PictureBox();
            this.Pic_Close = new System.Windows.Forms.PictureBox();
            this.pic_Profil = new System.Windows.Forms.PictureBox();
            this.pic_TSAMLogo = new System.Windows.Forms.PictureBox();
            this.lbl_EMAIL = new System.Windows.Forms.Label();
            this.lbl_ContentName = new System.Windows.Forms.Label();
            this.txt_ContentName = new System.Windows.Forms.TextBox();
            this.lbl_ContentType = new System.Windows.Forms.Label();
            this.lbl_ContentEpisodeNbr = new System.Windows.Forms.Label();
            this.txt_ContentEpisodeNbr = new System.Windows.Forms.TextBox();
            this.lbl_ContentSeasonNbr = new System.Windows.Forms.Label();
            this.txt_ContentSeasonNbr = new System.Windows.Forms.TextBox();
            this.lbl_ContentResume = new System.Windows.Forms.Label();
            this.txt_ContentResume = new System.Windows.Forms.RichTextBox();
            this.txt_ContentType = new System.Windows.Forms.TextBox();
            this.lbl_Producter = new System.Windows.Forms.Label();
            this.txt_Producter = new System.Windows.Forms.TextBox();
            this.rdb_Serie = new System.Windows.Forms.RadioButton();
            this.rdb_Anime = new System.Windows.Forms.RadioButton();
            this.btn_SendForm = new System.Windows.Forms.Button();
            this.lbl_ErrorMsg = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_Confirmationmsg = new System.Windows.Forms.Label();
            this.lbl_ErrorInName = new System.Windows.Forms.Label();
            this.lbl_ErrorInType = new System.Windows.Forms.Label();
            this.lbl_ErrorInNbrEpisodes = new System.Windows.Forms.Label();
            this.lbl_ErrorInNbrSeason = new System.Windows.Forms.Label();
            this.lbl_ErrorInProducer = new System.Windows.Forms.Label();
            this.lbl_ErrorInSynopsis = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lbl_ImageURL = new System.Windows.Forms.Label();
            this.txt_ImageURL = new System.Windows.Forms.TextBox();
            this.lbl_ErrorInURL = new System.Windows.Forms.Label();
            this.btn_Logout = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pic_CreateSeries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pic_Minimize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pic_Close)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Profil)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_TSAMLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // pic_CreateSeries
            // 
            this.pic_CreateSeries.BackColor = System.Drawing.Color.Transparent;
            this.pic_CreateSeries.Image = ((System.Drawing.Image)(resources.GetObject("pic_CreateSeries.Image")));
            this.pic_CreateSeries.Location = new System.Drawing.Point(240, 22);
            this.pic_CreateSeries.Name = "pic_CreateSeries";
            this.pic_CreateSeries.Size = new System.Drawing.Size(64, 60);
            this.pic_CreateSeries.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_CreateSeries.TabIndex = 23;
            this.pic_CreateSeries.TabStop = false;
            // 
            // Pic_Minimize
            // 
            this.Pic_Minimize.BackColor = System.Drawing.Color.Transparent;
            this.Pic_Minimize.Image = ((System.Drawing.Image)(resources.GetObject("Pic_Minimize.Image")));
            this.Pic_Minimize.Location = new System.Drawing.Point(859, 1);
            this.Pic_Minimize.Name = "Pic_Minimize";
            this.Pic_Minimize.Size = new System.Drawing.Size(47, 36);
            this.Pic_Minimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Pic_Minimize.TabIndex = 22;
            this.Pic_Minimize.TabStop = false;
            this.Pic_Minimize.LocationChanged += new System.EventHandler(this.Pic_Minimize_LocationChanged);
            this.Pic_Minimize.Click += new System.EventHandler(this.Pic_Minimize_Click);
            // 
            // Pic_Close
            // 
            this.Pic_Close.BackColor = System.Drawing.Color.Transparent;
            this.Pic_Close.Image = ((System.Drawing.Image)(resources.GetObject("Pic_Close.Image")));
            this.Pic_Close.Location = new System.Drawing.Point(912, 1);
            this.Pic_Close.Name = "Pic_Close";
            this.Pic_Close.Size = new System.Drawing.Size(48, 36);
            this.Pic_Close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Pic_Close.TabIndex = 21;
            this.Pic_Close.TabStop = false;
            this.Pic_Close.Click += new System.EventHandler(this.pictureBox7_Click);
            // 
            // pic_Profil
            // 
            this.pic_Profil.BackColor = System.Drawing.Color.Transparent;
            this.pic_Profil.Image = ((System.Drawing.Image)(resources.GetObject("pic_Profil.Image")));
            this.pic_Profil.InitialImage = null;
            this.pic_Profil.Location = new System.Drawing.Point(796, 12);
            this.pic_Profil.Name = "pic_Profil";
            this.pic_Profil.Size = new System.Drawing.Size(48, 44);
            this.pic_Profil.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_Profil.TabIndex = 20;
            this.pic_Profil.TabStop = false;
            // 
            // pic_TSAMLogo
            // 
            this.pic_TSAMLogo.BackColor = System.Drawing.Color.Transparent;
            this.pic_TSAMLogo.Image = ((System.Drawing.Image)(resources.GetObject("pic_TSAMLogo.Image")));
            this.pic_TSAMLogo.Location = new System.Drawing.Point(2, 1);
            this.pic_TSAMLogo.Name = "pic_TSAMLogo";
            this.pic_TSAMLogo.Size = new System.Drawing.Size(204, 81);
            this.pic_TSAMLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_TSAMLogo.TabIndex = 16;
            this.pic_TSAMLogo.TabStop = false;
            this.pic_TSAMLogo.Click += new System.EventHandler(this.pic_TSAMLogo_Click);
            // 
            // lbl_EMAIL
            // 
            this.lbl_EMAIL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_EMAIL.BackColor = System.Drawing.Color.Transparent;
            this.lbl_EMAIL.Location = new System.Drawing.Point(715, 60);
            this.lbl_EMAIL.Name = "lbl_EMAIL";
            this.lbl_EMAIL.Size = new System.Drawing.Size(129, 13);
            this.lbl_EMAIL.TabIndex = 15;
            this.lbl_EMAIL.Text = "qwertzu";
            this.lbl_EMAIL.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl_ContentName
            // 
            this.lbl_ContentName.AutoSize = true;
            this.lbl_ContentName.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ContentName.Font = new System.Drawing.Font("Rockwell", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ContentName.Location = new System.Drawing.Point(34, 126);
            this.lbl_ContentName.Name = "lbl_ContentName";
            this.lbl_ContentName.Size = new System.Drawing.Size(97, 16);
            this.lbl_ContentName.TabIndex = 24;
            this.lbl_ContentName.Text = "Nom de la série";
            // 
            // txt_ContentName
            // 
            this.txt_ContentName.Location = new System.Drawing.Point(37, 142);
            this.txt_ContentName.Name = "txt_ContentName";
            this.txt_ContentName.Size = new System.Drawing.Size(149, 20);
            this.txt_ContentName.TabIndex = 1;
            // 
            // lbl_ContentType
            // 
            this.lbl_ContentType.AutoSize = true;
            this.lbl_ContentType.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ContentType.Font = new System.Drawing.Font("Rockwell", 9.75F);
            this.lbl_ContentType.Location = new System.Drawing.Point(34, 203);
            this.lbl_ContentType.Name = "lbl_ContentType";
            this.lbl_ContentType.Size = new System.Drawing.Size(44, 16);
            this.lbl_ContentType.TabIndex = 26;
            this.lbl_ContentType.Text = "Genre";
            // 
            // lbl_ContentEpisodeNbr
            // 
            this.lbl_ContentEpisodeNbr.AutoSize = true;
            this.lbl_ContentEpisodeNbr.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ContentEpisodeNbr.Font = new System.Drawing.Font("Rockwell", 9.75F);
            this.lbl_ContentEpisodeNbr.Location = new System.Drawing.Point(34, 281);
            this.lbl_ContentEpisodeNbr.Name = "lbl_ContentEpisodeNbr";
            this.lbl_ContentEpisodeNbr.Size = new System.Drawing.Size(122, 16);
            this.lbl_ContentEpisodeNbr.TabIndex = 29;
            this.lbl_ContentEpisodeNbr.Text = "Nombre d\'épidodes";
            // 
            // txt_ContentEpisodeNbr
            // 
            this.txt_ContentEpisodeNbr.Location = new System.Drawing.Point(37, 297);
            this.txt_ContentEpisodeNbr.Name = "txt_ContentEpisodeNbr";
            this.txt_ContentEpisodeNbr.Size = new System.Drawing.Size(149, 20);
            this.txt_ContentEpisodeNbr.TabIndex = 3;
            // 
            // lbl_ContentSeasonNbr
            // 
            this.lbl_ContentSeasonNbr.AutoSize = true;
            this.lbl_ContentSeasonNbr.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ContentSeasonNbr.Font = new System.Drawing.Font("Rockwell", 9.75F);
            this.lbl_ContentSeasonNbr.Location = new System.Drawing.Point(34, 362);
            this.lbl_ContentSeasonNbr.Name = "lbl_ContentSeasonNbr";
            this.lbl_ContentSeasonNbr.Size = new System.Drawing.Size(118, 16);
            this.lbl_ContentSeasonNbr.TabIndex = 31;
            this.lbl_ContentSeasonNbr.Text = "Nombre de saisons";
            // 
            // txt_ContentSeasonNbr
            // 
            this.txt_ContentSeasonNbr.Location = new System.Drawing.Point(37, 378);
            this.txt_ContentSeasonNbr.Name = "txt_ContentSeasonNbr";
            this.txt_ContentSeasonNbr.Size = new System.Drawing.Size(149, 20);
            this.txt_ContentSeasonNbr.TabIndex = 4;
            // 
            // lbl_ContentResume
            // 
            this.lbl_ContentResume.AutoSize = true;
            this.lbl_ContentResume.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ContentResume.Font = new System.Drawing.Font("Rockwell", 9.75F);
            this.lbl_ContentResume.Location = new System.Drawing.Point(288, 219);
            this.lbl_ContentResume.Name = "lbl_ContentResume";
            this.lbl_ContentResume.Size = new System.Drawing.Size(51, 16);
            this.lbl_ContentResume.TabIndex = 33;
            this.lbl_ContentResume.Text = "résumé";
            // 
            // txt_ContentResume
            // 
            this.txt_ContentResume.Location = new System.Drawing.Point(291, 236);
            this.txt_ContentResume.Name = "txt_ContentResume";
            this.txt_ContentResume.Size = new System.Drawing.Size(406, 142);
            this.txt_ContentResume.TabIndex = 8;
            this.txt_ContentResume.Text = "";
            // 
            // txt_ContentType
            // 
            this.txt_ContentType.Location = new System.Drawing.Point(37, 219);
            this.txt_ContentType.Name = "txt_ContentType";
            this.txt_ContentType.Size = new System.Drawing.Size(149, 20);
            this.txt_ContentType.TabIndex = 2;
            // 
            // lbl_Producter
            // 
            this.lbl_Producter.AutoSize = true;
            this.lbl_Producter.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Producter.Font = new System.Drawing.Font("Rockwell", 9.75F);
            this.lbl_Producter.Location = new System.Drawing.Point(37, 440);
            this.lbl_Producter.Name = "lbl_Producter";
            this.lbl_Producter.Size = new System.Drawing.Size(114, 16);
            this.lbl_Producter.TabIndex = 36;
            this.lbl_Producter.Text = "Réalisateur/Studio";
            // 
            // txt_Producter
            // 
            this.txt_Producter.Location = new System.Drawing.Point(37, 457);
            this.txt_Producter.Name = "txt_Producter";
            this.txt_Producter.Size = new System.Drawing.Size(149, 20);
            this.txt_Producter.TabIndex = 5;
            // 
            // rdb_Serie
            // 
            this.rdb_Serie.AutoSize = true;
            this.rdb_Serie.BackColor = System.Drawing.Color.Transparent;
            this.rdb_Serie.Font = new System.Drawing.Font("Rockwell", 9.75F);
            this.rdb_Serie.Location = new System.Drawing.Point(291, 145);
            this.rdb_Serie.Name = "rdb_Serie";
            this.rdb_Serie.Size = new System.Drawing.Size(55, 20);
            this.rdb_Serie.TabIndex = 6;
            this.rdb_Serie.TabStop = true;
            this.rdb_Serie.Text = "Série";
            this.rdb_Serie.UseVisualStyleBackColor = false;
            // 
            // rdb_Anime
            // 
            this.rdb_Anime.AutoSize = true;
            this.rdb_Anime.BackColor = System.Drawing.Color.Transparent;
            this.rdb_Anime.Font = new System.Drawing.Font("Rockwell", 9.75F);
            this.rdb_Anime.Location = new System.Drawing.Point(291, 166);
            this.rdb_Anime.Name = "rdb_Anime";
            this.rdb_Anime.Size = new System.Drawing.Size(63, 20);
            this.rdb_Anime.TabIndex = 7;
            this.rdb_Anime.TabStop = true;
            this.rdb_Anime.Text = "Anime";
            this.rdb_Anime.UseVisualStyleBackColor = false;
            // 
            // btn_SendForm
            // 
            this.btn_SendForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_SendForm.Location = new System.Drawing.Point(742, 457);
            this.btn_SendForm.Name = "btn_SendForm";
            this.btn_SendForm.Size = new System.Drawing.Size(141, 40);
            this.btn_SendForm.TabIndex = 9;
            this.btn_SendForm.Text = "Envoyer";
            this.btn_SendForm.UseVisualStyleBackColor = true;
            this.btn_SendForm.Click += new System.EventHandler(this.btn_SendForm_Click);
            // 
            // lbl_ErrorMsg
            // 
            this.lbl_ErrorMsg.AutoSize = true;
            this.lbl_ErrorMsg.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ErrorMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ErrorMsg.ForeColor = System.Drawing.Color.Maroon;
            this.lbl_ErrorMsg.Location = new System.Drawing.Point(296, 503);
            this.lbl_ErrorMsg.Name = "lbl_ErrorMsg";
            this.lbl_ErrorMsg.Size = new System.Drawing.Size(81, 20);
            this.lbl_ErrorMsg.TabIndex = 41;
            this.lbl_ErrorMsg.Text = "ErrorMSG";
            this.lbl_ErrorMsg.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Rockwell", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(336, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(270, 27);
            this.label1.TabIndex = 42;
            this.label1.Text = "Ajout de séries/animes";
            // 
            // lbl_Confirmationmsg
            // 
            this.lbl_Confirmationmsg.AutoSize = true;
            this.lbl_Confirmationmsg.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Confirmationmsg.Font = new System.Drawing.Font("Rockwell", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Confirmationmsg.ForeColor = System.Drawing.Color.Green;
            this.lbl_Confirmationmsg.Location = new System.Drawing.Point(297, 506);
            this.lbl_Confirmationmsg.Name = "lbl_Confirmationmsg";
            this.lbl_Confirmationmsg.Size = new System.Drawing.Size(359, 17);
            this.lbl_Confirmationmsg.TabIndex = 43;
            this.lbl_Confirmationmsg.Text = "Le contenu à bien été ajouté à la base de donnée";
            this.lbl_Confirmationmsg.Visible = false;
            // 
            // lbl_ErrorInName
            // 
            this.lbl_ErrorInName.AutoSize = true;
            this.lbl_ErrorInName.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ErrorInName.Font = new System.Drawing.Font("Rockwell", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ErrorInName.ForeColor = System.Drawing.Color.Maroon;
            this.lbl_ErrorInName.Location = new System.Drawing.Point(37, 165);
            this.lbl_ErrorInName.Name = "lbl_ErrorInName";
            this.lbl_ErrorInName.Size = new System.Drawing.Size(218, 16);
            this.lbl_ErrorInName.TabIndex = 44;
            this.lbl_ErrorInName.Text = "Veuillez remplir le champs ci-dessus";
            this.lbl_ErrorInName.Visible = false;
            // 
            // lbl_ErrorInType
            // 
            this.lbl_ErrorInType.AutoSize = true;
            this.lbl_ErrorInType.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ErrorInType.Font = new System.Drawing.Font("Rockwell", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ErrorInType.ForeColor = System.Drawing.Color.Maroon;
            this.lbl_ErrorInType.Location = new System.Drawing.Point(37, 242);
            this.lbl_ErrorInType.Name = "lbl_ErrorInType";
            this.lbl_ErrorInType.Size = new System.Drawing.Size(218, 16);
            this.lbl_ErrorInType.TabIndex = 45;
            this.lbl_ErrorInType.Text = "Veuillez remplir le champs ci-dessus";
            this.lbl_ErrorInType.Visible = false;
            // 
            // lbl_ErrorInNbrEpisodes
            // 
            this.lbl_ErrorInNbrEpisodes.AutoSize = true;
            this.lbl_ErrorInNbrEpisodes.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ErrorInNbrEpisodes.Font = new System.Drawing.Font("Rockwell", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ErrorInNbrEpisodes.ForeColor = System.Drawing.Color.Maroon;
            this.lbl_ErrorInNbrEpisodes.Location = new System.Drawing.Point(34, 320);
            this.lbl_ErrorInNbrEpisodes.Name = "lbl_ErrorInNbrEpisodes";
            this.lbl_ErrorInNbrEpisodes.Size = new System.Drawing.Size(218, 16);
            this.lbl_ErrorInNbrEpisodes.TabIndex = 46;
            this.lbl_ErrorInNbrEpisodes.Text = "Veuillez remplir le champs ci-dessus";
            this.lbl_ErrorInNbrEpisodes.Visible = false;
            // 
            // lbl_ErrorInNbrSeason
            // 
            this.lbl_ErrorInNbrSeason.AutoSize = true;
            this.lbl_ErrorInNbrSeason.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ErrorInNbrSeason.Font = new System.Drawing.Font("Rockwell", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ErrorInNbrSeason.ForeColor = System.Drawing.Color.Maroon;
            this.lbl_ErrorInNbrSeason.Location = new System.Drawing.Point(37, 401);
            this.lbl_ErrorInNbrSeason.Name = "lbl_ErrorInNbrSeason";
            this.lbl_ErrorInNbrSeason.Size = new System.Drawing.Size(218, 16);
            this.lbl_ErrorInNbrSeason.TabIndex = 47;
            this.lbl_ErrorInNbrSeason.Text = "Veuillez remplir le champs ci-dessus";
            this.lbl_ErrorInNbrSeason.Visible = false;
            // 
            // lbl_ErrorInProducer
            // 
            this.lbl_ErrorInProducer.AutoSize = true;
            this.lbl_ErrorInProducer.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ErrorInProducer.Font = new System.Drawing.Font("Rockwell", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ErrorInProducer.ForeColor = System.Drawing.Color.Maroon;
            this.lbl_ErrorInProducer.Location = new System.Drawing.Point(34, 484);
            this.lbl_ErrorInProducer.Name = "lbl_ErrorInProducer";
            this.lbl_ErrorInProducer.Size = new System.Drawing.Size(218, 16);
            this.lbl_ErrorInProducer.TabIndex = 48;
            this.lbl_ErrorInProducer.Text = "Veuillez remplir le champs ci-dessus";
            this.lbl_ErrorInProducer.Visible = false;
            // 
            // lbl_ErrorInSynopsis
            // 
            this.lbl_ErrorInSynopsis.AutoSize = true;
            this.lbl_ErrorInSynopsis.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ErrorInSynopsis.Font = new System.Drawing.Font("Rockwell", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ErrorInSynopsis.ForeColor = System.Drawing.Color.Maroon;
            this.lbl_ErrorInSynopsis.Location = new System.Drawing.Point(288, 392);
            this.lbl_ErrorInSynopsis.Name = "lbl_ErrorInSynopsis";
            this.lbl_ErrorInSynopsis.Size = new System.Drawing.Size(218, 16);
            this.lbl_ErrorInSynopsis.TabIndex = 49;
            this.lbl_ErrorInSynopsis.Text = "Veuillez remplir le champs ci-dessus";
            this.lbl_ErrorInSynopsis.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(-23, -46);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 50);
            this.pictureBox1.TabIndex = 50;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(-23, -46);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(100, 50);
            this.pictureBox2.TabIndex = 51;
            this.pictureBox2.TabStop = false;
            // 
            // lbl_ImageURL
            // 
            this.lbl_ImageURL.AutoSize = true;
            this.lbl_ImageURL.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ImageURL.Location = new System.Drawing.Point(297, 440);
            this.lbl_ImageURL.Name = "lbl_ImageURL";
            this.lbl_ImageURL.Size = new System.Drawing.Size(79, 13);
            this.lbl_ImageURL.TabIndex = 52;
            this.lbl_ImageURL.Text = "URL de l\'image";
            this.lbl_ImageURL.Click += new System.EventHandler(this.lbl_ImageURL_Click);
            // 
            // txt_ImageURL
            // 
            this.txt_ImageURL.Location = new System.Drawing.Point(300, 457);
            this.txt_ImageURL.Name = "txt_ImageURL";
            this.txt_ImageURL.Size = new System.Drawing.Size(223, 20);
            this.txt_ImageURL.TabIndex = 53;
            // 
            // lbl_ErrorInURL
            // 
            this.lbl_ErrorInURL.AutoSize = true;
            this.lbl_ErrorInURL.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ErrorInURL.Font = new System.Drawing.Font("Rockwell", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ErrorInURL.ForeColor = System.Drawing.Color.Maroon;
            this.lbl_ErrorInURL.Location = new System.Drawing.Point(297, 484);
            this.lbl_ErrorInURL.Name = "lbl_ErrorInURL";
            this.lbl_ErrorInURL.Size = new System.Drawing.Size(218, 16);
            this.lbl_ErrorInURL.TabIndex = 54;
            this.lbl_ErrorInURL.Text = "Veuillez remplir le champs ci-dessus";
            this.lbl_ErrorInURL.Visible = false;
            // 
            // btn_Logout
            // 
            this.btn_Logout.BackColor = System.Drawing.SystemColors.Control;
            this.btn_Logout.Font = new System.Drawing.Font("Rockwell", 9.75F);
            this.btn_Logout.Location = new System.Drawing.Point(859, 43);
            this.btn_Logout.Name = "btn_Logout";
            this.btn_Logout.Size = new System.Drawing.Size(105, 30);
            this.btn_Logout.TabIndex = 55;
            this.btn_Logout.Text = "Deconnection";
            this.btn_Logout.UseVisualStyleBackColor = false;
            this.btn_Logout.Click += new System.EventHandler(this.btn_Logout_Click);
            // 
            // frm_CreateSeries
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(966, 532);
            this.ControlBox = false;
            this.Controls.Add(this.btn_Logout);
            this.Controls.Add(this.lbl_ErrorInURL);
            this.Controls.Add(this.txt_ImageURL);
            this.Controls.Add(this.lbl_ImageURL);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lbl_ErrorInSynopsis);
            this.Controls.Add(this.lbl_ErrorInProducer);
            this.Controls.Add(this.lbl_ErrorInNbrSeason);
            this.Controls.Add(this.lbl_ErrorInNbrEpisodes);
            this.Controls.Add(this.lbl_ErrorInType);
            this.Controls.Add(this.lbl_ErrorInName);
            this.Controls.Add(this.lbl_Confirmationmsg);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbl_ErrorMsg);
            this.Controls.Add(this.btn_SendForm);
            this.Controls.Add(this.rdb_Anime);
            this.Controls.Add(this.rdb_Serie);
            this.Controls.Add(this.txt_Producter);
            this.Controls.Add(this.lbl_Producter);
            this.Controls.Add(this.txt_ContentType);
            this.Controls.Add(this.txt_ContentResume);
            this.Controls.Add(this.lbl_ContentResume);
            this.Controls.Add(this.txt_ContentSeasonNbr);
            this.Controls.Add(this.lbl_ContentSeasonNbr);
            this.Controls.Add(this.txt_ContentEpisodeNbr);
            this.Controls.Add(this.lbl_ContentEpisodeNbr);
            this.Controls.Add(this.lbl_ContentType);
            this.Controls.Add(this.txt_ContentName);
            this.Controls.Add(this.lbl_ContentName);
            this.Controls.Add(this.pic_CreateSeries);
            this.Controls.Add(this.Pic_Minimize);
            this.Controls.Add(this.Pic_Close);
            this.Controls.Add(this.pic_Profil);
            this.Controls.Add(this.pic_TSAMLogo);
            this.Controls.Add(this.lbl_EMAIL);
            this.Name = "frm_CreateSeries";
            this.Text = " ";
            this.Load += new System.EventHandler(this.frm_CreateSeries_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pic_CreateSeries)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pic_Minimize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pic_Close)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Profil)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_TSAMLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pic_CreateSeries;
        private System.Windows.Forms.PictureBox Pic_Minimize;
        private System.Windows.Forms.PictureBox Pic_Close;
        private System.Windows.Forms.PictureBox pic_Profil;
        private System.Windows.Forms.PictureBox pic_TSAMLogo;
        private System.Windows.Forms.Label lbl_EMAIL;
        private System.Windows.Forms.Label lbl_ContentName;
        private System.Windows.Forms.TextBox txt_ContentName;
        private System.Windows.Forms.Label lbl_ContentType;
        private System.Windows.Forms.Label lbl_ContentEpisodeNbr;
        private System.Windows.Forms.TextBox txt_ContentEpisodeNbr;
        private System.Windows.Forms.Label lbl_ContentSeasonNbr;
        private System.Windows.Forms.TextBox txt_ContentSeasonNbr;
        private System.Windows.Forms.Label lbl_ContentResume;
        private System.Windows.Forms.RichTextBox txt_ContentResume;
        private System.Windows.Forms.TextBox txt_ContentType;
        private System.Windows.Forms.Label lbl_Producter;
        private System.Windows.Forms.TextBox txt_Producter;
        private System.Windows.Forms.RadioButton rdb_Serie;
        private System.Windows.Forms.RadioButton rdb_Anime;
        private System.Windows.Forms.Button btn_SendForm;
        private System.Windows.Forms.Label lbl_ErrorMsg;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_Confirmationmsg;
        private System.Windows.Forms.Label lbl_ErrorInName;
        private System.Windows.Forms.Label lbl_ErrorInType;
        private System.Windows.Forms.Label lbl_ErrorInNbrEpisodes;
        private System.Windows.Forms.Label lbl_ErrorInNbrSeason;
        private System.Windows.Forms.Label lbl_ErrorInProducer;
        private System.Windows.Forms.Label lbl_ErrorInSynopsis;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lbl_ImageURL;
        private System.Windows.Forms.TextBox txt_ImageURL;
        private System.Windows.Forms.Label lbl_ErrorInURL;
        private System.Windows.Forms.Button btn_Logout;
    }
}