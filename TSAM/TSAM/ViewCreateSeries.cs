﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TSAM.Model;
using TSAM.IHM;


namespace TSAM
{
    public partial class frm_CreateSeries : Form
    {

        User connectedUser;

        /// <summary>
        ///when the form is call all the user info are passed thought the constructor
        ///the user email is displayed
        /// </summary>
        /// <param name="connectedUser"></param>
        public frm_CreateSeries(User connectedUser)
        {
            this.connectedUser = connectedUser;
            InitializeComponent();
            lbl_EMAIL.Text = connectedUser.Mail;
            rdb_Serie.Checked = true;
        }
        /// <summary>
        /// Designed to close the  form when click on the close bouton
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox7_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// minimise the form when click on the minimize Bouton
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Pic_Minimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        /// <summary>
        /// close the actual form when opening a new one
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void f_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// save the location of the form on the ocnfig file when moving the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Pic_Minimize_LocationChanged(object sender, EventArgs e)
        {
            Config configloader = new Config();
            configloader.Load();
            configloader.app_Heigh = this.Location.Y;
            configloader.app_Width = this.Location.X;
            configloader.Save();
        }

        /// <summary>
        /// on load, open the config fil and set the form location with the parameter in the config file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frm_CreateSeries_Load(object sender, EventArgs e)
        {
            Config startConfig = new Config();
            startConfig.Load();
            this.Location = new Point(startConfig.app_Width, startConfig.app_Heigh);
        }
        
        /// <summary>
        /// open the main admin page when click on the logo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pic_TSAMLogo_Click(object sender, EventArgs e)
        {
            
            frm_AdminLogged f = new frm_AdminLogged(connectedUser);
            f.FormClosing += new FormClosingEventHandler(f_FormClosing);
            this.Hide();
            f.Show();
        }


        /// <summary>
        /// on click: send all the information in the field and catch the potential error
        /// when the info are all clear they are input in the database and all the field become empty
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_SendForm_Click(object sender, EventArgs e)
        {
            txt_ContentName.BackColor = Color.White;
            txt_ContentType.BackColor = Color.White;
            txt_ContentEpisodeNbr.BackColor = Color.White;
            txt_ContentSeasonNbr.BackColor = Color.White;
            txt_Producter.BackColor = Color.White;
            txt_ContentResume.BackColor = Color.White;
            txt_ImageURL.BackColor = Color.White;

            lbl_ErrorMsg.Visible = false;
            lbl_Confirmationmsg.Visible = false;
            lbl_ErrorInName.Visible = false;
            lbl_ErrorInNbrEpisodes.Visible = false;
            lbl_ErrorInNbrSeason.Visible = false;
            lbl_ErrorInProducer.Visible = false;
            lbl_ErrorInSynopsis.Visible = false;
            lbl_ErrorInType.Visible = false;
            lbl_ErrorInURL.Visible = false;
            

            bool IsContentAnANime = false;
            bool errorFlag = false;

            if(rdb_Anime.Checked == true)
            {
                IsContentAnANime = true;
            }

            int nbrEpisodes = -1;
            int nbrSeasons = -1;

            bool errorInEpisodeFlag = false;
            bool errorInSeasonFlag = false;
            
            try
            {
                 nbrEpisodes = Int32.Parse(txt_ContentEpisodeNbr.Text);
                
                if(nbrEpisodes <= 0)
                {
                    txt_ContentEpisodeNbr.BackColor = Color.LightPink;
                    errorInEpisodeFlag = true;
                    lbl_ErrorInNbrEpisodes.Text = "Le nombre ne peut pas être négatif";
                }
            }
            catch (FormatException)
            {
                if(txt_ContentEpisodeNbr.Text == "")
                {
                    lbl_ErrorInNbrEpisodes.Text = "Veuillez remplir le champs ci-dessus";
                }
                else
                {
                    lbl_ErrorInNbrEpisodes.Text = "Veuillez entrez uniquement des chiffres";
                }
                txt_ContentEpisodeNbr.BackColor = Color.LightPink;
                errorInEpisodeFlag = true;
            }
            catch (OverflowException)
            {
                txt_ContentEpisodeNbr.BackColor = Color.LightPink;
                errorInEpisodeFlag = true;
                lbl_ErrorInNbrEpisodes.Text = "Le nombre écrit est trop grand";
            }

            if(errorInEpisodeFlag == true)
            {
                errorFlag = true;
                lbl_ErrorInNbrEpisodes.Visible = true;
            }

            try
            {
                nbrSeasons = Int32.Parse(txt_ContentSeasonNbr.Text);

                if(nbrSeasons <= 0)
                {
                    txt_ContentSeasonNbr.BackColor = Color.LightPink;
                    errorInSeasonFlag = true;
                    lbl_ErrorInNbrSeason.Text = "Le nombre ne peut pas être négatif";
                }

            }
            catch (FormatException)
            {
                if(txt_ContentSeasonNbr.Text == "")
                {
                    lbl_ErrorInNbrSeason.Text = "Veuillez remplir le champs ci-dessus";

                }
                else
                {
                    lbl_ErrorInNbrSeason.Text = "Veuillez entrez uniquement des chiffres";

                }
                txt_ContentSeasonNbr.BackColor = Color.LightPink;
                errorInSeasonFlag = true;
            }
            catch (OverflowException)
            {
                txt_ContentSeasonNbr.BackColor = Color.LightPink;
                errorInSeasonFlag = true;
                lbl_ErrorInNbrSeason.Text = "Le nombre écrit est trop grand";
            }

            if(errorInSeasonFlag == true)
            {
                lbl_ErrorInNbrSeason.Visible = true;
                errorFlag = true;
            }

     


            try
            {
                Content seriesToCreate = new Content();
                seriesToCreate.Create(txt_ContentName.Text, txt_ContentType.Text, nbrEpisodes, nbrSeasons, txt_Producter.Text, IsContentAnANime, txt_ContentResume.Text, txt_ImageURL.Text);
            }
            catch (Exception ErrorInForm)
            {
                errorFlag = true;
                if (ErrorInForm.Message.Contains("name"))
                {
                    txt_ContentName.BackColor = Color.LightPink;
                    lbl_ErrorInName.Visible = true;
                }
                if (ErrorInForm.Message.Contains("errorintype"))
                {
                    txt_ContentType.BackColor = Color.LightPink;
                    lbl_ErrorInType.Visible = true;
                }
                if (ErrorInForm.Message.Contains("producer"))
                {
                    txt_Producter.BackColor = Color.LightPink;
                    lbl_ErrorInProducer.Visible = true;
                }
                if (ErrorInForm.Message.Contains("synopsis"))
                {
                    txt_ContentResume.BackColor = Color.LightPink;
                    lbl_ErrorInSynopsis.Visible = true;
                }
                if (ErrorInForm.Message.Contains("notUrl"))
                {
                    txt_ImageURL.BackColor = Color.LightPink;
                    lbl_ErrorInURL.Visible = true;
                }
            }
            
            
            

            if(errorFlag == false)
            {
                txt_ContentName.Text = "";
                txt_ContentType.Text = "";
                txt_ContentEpisodeNbr.Text = "";
                txt_ContentSeasonNbr.Text = "";
                txt_Producter.Text = "";
                txt_ContentResume.Text = "";
                txt_ImageURL.Text = "";
                rdb_Serie.Checked = true;
                lbl_Confirmationmsg.Visible = true;
            }
            else if(errorFlag == true)
            {
                Shake(this);
                lbl_ErrorMsg.Text = "certain champs n'ont pas été remplis correctement";
                lbl_ErrorMsg.Visible = true;
            }



        }

       

        /// <summary>
        /// when an error is found in the creation on a content the form shake to show to the user that an error as been found
        /// </summary>
        /// <param name="form"></param>
        private static void Shake(Form form)
        {
            var original = form.Location;
            var rnd = new Random(1337);
            const int shake_amplitude = 10;
            for (int i = 0; i < 10; i++)
            {
                form.Location = new Point(original.X + rnd.Next(-shake_amplitude, shake_amplitude), original.Y + rnd.Next(-shake_amplitude, shake_amplitude));
                System.Threading.Thread.Sleep(20);
            }
            form.Location = original;
        }

        private void lbl_ImageURL_Click(object sender, EventArgs e)
        {

        }

        private void btn_Logout_Click(object sender, EventArgs e)
        {
            frm_Login f = new frm_Login();
            f.FormClosing += new FormClosingEventHandler(f_FormClosing);
            this.Hide();
            f.Show();
        }
    }
}
