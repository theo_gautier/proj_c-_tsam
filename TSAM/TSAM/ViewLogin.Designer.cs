﻿namespace TSAM.IHM
{
    partial class frm_Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Login));
            this.lblLogin = new System.Windows.Forms.Label();
            this.lblEnterMail = new System.Windows.Forms.Label();
            this.lblEnterPsw = new System.Windows.Forms.Label();
            this.btn_ValidateLogin = new System.Windows.Forms.Button();
            this.btnToRegister = new System.Windows.Forms.Button();
            this.txt_EntryEmail = new System.Windows.Forms.TextBox();
            this.txt_EntryPSW = new System.Windows.Forms.TextBox();
            this.lbl_error = new System.Windows.Forms.Label();
            this.pic_TSAMLogo = new System.Windows.Forms.PictureBox();
            this.pic_Close = new System.Windows.Forms.PictureBox();
            this.pic_Minimize = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pic_TSAMLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Close)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Minimize)).BeginInit();
            this.SuspendLayout();
            // 
            // lblLogin
            // 
            this.lblLogin.AutoSize = true;
            this.lblLogin.BackColor = System.Drawing.Color.Transparent;
            this.lblLogin.Font = new System.Drawing.Font("Rockwell", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLogin.Location = new System.Drawing.Point(142, 116);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.Size = new System.Drawing.Size(107, 39);
            this.lblLogin.TabIndex = 0;
            this.lblLogin.Text = "Login";
            // 
            // lblEnterMail
            // 
            this.lblEnterMail.AutoSize = true;
            this.lblEnterMail.BackColor = System.Drawing.Color.Transparent;
            this.lblEnterMail.Font = new System.Drawing.Font("Rockwell", 9.75F);
            this.lblEnterMail.Location = new System.Drawing.Point(146, 169);
            this.lblEnterMail.Name = "lblEnterMail";
            this.lblEnterMail.Size = new System.Drawing.Size(112, 16);
            this.lblEnterMail.TabIndex = 1;
            this.lblEnterMail.Text = "Entrez votre email";
            // 
            // lblEnterPsw
            // 
            this.lblEnterPsw.AutoSize = true;
            this.lblEnterPsw.BackColor = System.Drawing.Color.Transparent;
            this.lblEnterPsw.Font = new System.Drawing.Font("Rockwell", 9.75F);
            this.lblEnterPsw.Location = new System.Drawing.Point(146, 244);
            this.lblEnterPsw.Name = "lblEnterPsw";
            this.lblEnterPsw.Size = new System.Drawing.Size(85, 16);
            this.lblEnterPsw.TabIndex = 2;
            this.lblEnterPsw.Text = "Mot de passe";
            // 
            // btn_ValidateLogin
            // 
            this.btn_ValidateLogin.Location = new System.Drawing.Point(231, 337);
            this.btn_ValidateLogin.Name = "btn_ValidateLogin";
            this.btn_ValidateLogin.Size = new System.Drawing.Size(131, 26);
            this.btn_ValidateLogin.TabIndex = 3;
            this.btn_ValidateLogin.Text = "Valider votre connexion";
            this.btn_ValidateLogin.UseVisualStyleBackColor = true;
            this.btn_ValidateLogin.Click += new System.EventHandler(this.btn_ValidateLogin_Click);
            // 
            // btnToRegister
            // 
            this.btnToRegister.Location = new System.Drawing.Point(287, 369);
            this.btnToRegister.Name = "btnToRegister";
            this.btnToRegister.Size = new System.Drawing.Size(75, 23);
            this.btnToRegister.TabIndex = 4;
            this.btnToRegister.Text = "S\'inscrire";
            this.btnToRegister.UseVisualStyleBackColor = true;
            this.btnToRegister.Click += new System.EventHandler(this.btnToRegister_Click);
            // 
            // txt_EntryEmail
            // 
            this.txt_EntryEmail.BackColor = System.Drawing.SystemColors.Control;
            this.txt_EntryEmail.Location = new System.Drawing.Point(149, 185);
            this.txt_EntryEmail.Name = "txt_EntryEmail";
            this.txt_EntryEmail.Size = new System.Drawing.Size(213, 20);
            this.txt_EntryEmail.TabIndex = 1;
            // 
            // txt_EntryPSW
            // 
            this.txt_EntryPSW.Location = new System.Drawing.Point(149, 260);
            this.txt_EntryPSW.Name = "txt_EntryPSW";
            this.txt_EntryPSW.ShortcutsEnabled = false;
            this.txt_EntryPSW.Size = new System.Drawing.Size(213, 20);
            this.txt_EntryPSW.TabIndex = 2;
            this.txt_EntryPSW.TextChanged += new System.EventHandler(this.txt_EntryPSW_TextChanged);
            // 
            // lbl_error
            // 
            this.lbl_error.AutoSize = true;
            this.lbl_error.BackColor = System.Drawing.Color.Transparent;
            this.lbl_error.Font = new System.Drawing.Font("Rockwell", 9.75F);
            this.lbl_error.ForeColor = System.Drawing.Color.Maroon;
            this.lbl_error.Location = new System.Drawing.Point(146, 307);
            this.lbl_error.Name = "lbl_error";
            this.lbl_error.Size = new System.Drawing.Size(244, 16);
            this.lbl_error.TabIndex = 7;
            this.lbl_error.Text = "Veuillez entrez une adresse email valable";
            this.lbl_error.Visible = false;
            // 
            // pic_TSAMLogo
            // 
            this.pic_TSAMLogo.BackColor = System.Drawing.Color.Transparent;
            this.pic_TSAMLogo.Image = ((System.Drawing.Image)(resources.GetObject("pic_TSAMLogo.Image")));
            this.pic_TSAMLogo.Location = new System.Drawing.Point(12, 12);
            this.pic_TSAMLogo.Name = "pic_TSAMLogo";
            this.pic_TSAMLogo.Size = new System.Drawing.Size(161, 74);
            this.pic_TSAMLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_TSAMLogo.TabIndex = 8;
            this.pic_TSAMLogo.TabStop = false;
            // 
            // pic_Close
            // 
            this.pic_Close.BackColor = System.Drawing.Color.Transparent;
            this.pic_Close.Image = ((System.Drawing.Image)(resources.GetObject("pic_Close.Image")));
            this.pic_Close.Location = new System.Drawing.Point(434, 1);
            this.pic_Close.Name = "pic_Close";
            this.pic_Close.Size = new System.Drawing.Size(48, 36);
            this.pic_Close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_Close.TabIndex = 9;
            this.pic_Close.TabStop = false;
            this.pic_Close.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pic_Minimize
            // 
            this.pic_Minimize.BackColor = System.Drawing.Color.Transparent;
            this.pic_Minimize.Image = ((System.Drawing.Image)(resources.GetObject("pic_Minimize.Image")));
            this.pic_Minimize.Location = new System.Drawing.Point(381, 1);
            this.pic_Minimize.Name = "pic_Minimize";
            this.pic_Minimize.Size = new System.Drawing.Size(47, 36);
            this.pic_Minimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_Minimize.TabIndex = 10;
            this.pic_Minimize.TabStop = false;
            this.pic_Minimize.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // frm_Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(484, 461);
            this.ControlBox = false;
            this.Controls.Add(this.pic_Minimize);
            this.Controls.Add(this.pic_Close);
            this.Controls.Add(this.pic_TSAMLogo);
            this.Controls.Add(this.lbl_error);
            this.Controls.Add(this.txt_EntryPSW);
            this.Controls.Add(this.txt_EntryEmail);
            this.Controls.Add(this.btnToRegister);
            this.Controls.Add(this.btn_ValidateLogin);
            this.Controls.Add(this.lblEnterPsw);
            this.Controls.Add(this.lblEnterMail);
            this.Controls.Add(this.lblLogin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Location = new System.Drawing.Point(200, 150);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_Login";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = " ";
            this.Load += new System.EventHandler(this.FrmLogin_Load);
            this.LocationChanged += new System.EventHandler(this.frm_Login_LocationChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pic_TSAMLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Close)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Minimize)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblLogin;
        private System.Windows.Forms.Label lblEnterMail;
        private System.Windows.Forms.Label lblEnterPsw;
        private System.Windows.Forms.Button btn_ValidateLogin;
        private System.Windows.Forms.Button btnToRegister;
        private System.Windows.Forms.TextBox txt_EntryEmail;
        private System.Windows.Forms.TextBox txt_EntryPSW;
        private System.Windows.Forms.Label lbl_error;
        private System.Windows.Forms.PictureBox pic_TSAMLogo;
        private System.Windows.Forms.PictureBox pic_Close;
        private System.Windows.Forms.PictureBox pic_Minimize;
    }
}