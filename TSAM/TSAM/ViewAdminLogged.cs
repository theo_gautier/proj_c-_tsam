﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TSAM.Model;
using TSAM.IHM;

namespace TSAM
{
    public partial class frm_AdminLogged : Form
    {
        User connectedUser;

        /// <summary>
        /// when the form is call all the user info are passed thought the constructor
        /// the user email is displayed
        /// </summary>
        /// <param name="connectedUser"></param>
        public frm_AdminLogged(User connectedUser)
        {
            InitializeComponent();
            this.connectedUser = connectedUser;

            lbl_EMAIL.Text = connectedUser.Mail;
            

        }

        /// <summary>
        /// on clicK: open the createseries form and close this one by calling f_FormClosing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pic_CreateSeries_Click(object sender, EventArgs e)
        {
            
            frm_CreateSeries f = new frm_CreateSeries(connectedUser);
            f.FormClosing += new FormClosingEventHandler(f_FormClosing);
            this.Hide();
            f.Show();
        }

        /// <summary>
        /// close the actual form when another one is being displayed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void f_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// save the location of the form on the ocnfig file when moving the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frm_AdminLogged_LocationChanged(object sender, EventArgs e)
        {
            Config configloader = new Config();
            configloader.Load();
            configloader.app_Heigh = this.Location.Y;
            configloader.app_Width = this.Location.X;
            configloader.Save();
        }

        /// <summary>
        /// on load, open the config fil and set the form location with the parameter in the config file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frm_AdminLogged_Load(object sender, EventArgs e)
        {
            Config startConfig = new Config();
            startConfig.Load();
            this.Location = new Point(startConfig.app_Width, startConfig.app_Heigh);
        }

        /// <summary>
        /// close the form when the closing buton is clicked on
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Pic_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// minimize the form when the minimize buton is clicked on
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Pic_Minimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btn_Logout_Click(object sender, EventArgs e)
        {
            frm_Login f = new frm_Login();
            f.FormClosing += new FormClosingEventHandler(f_FormClosing);
            this.Hide();
            f.Show();
        }
    }
}
