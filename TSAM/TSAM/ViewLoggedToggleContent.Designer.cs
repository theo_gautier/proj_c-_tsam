﻿namespace TSAM.IHM
{
    partial class frm_LoggedToggleContent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_LoggedToggleContent));
            this.lbl_UserEmail = new System.Windows.Forms.Label();
            this.lbl_EMAIL = new System.Windows.Forms.Label();
            this.pic_TSAMLogo = new System.Windows.Forms.PictureBox();
            this.pic_Search = new System.Windows.Forms.PictureBox();
            this.pic_News = new System.Windows.Forms.PictureBox();
            this.pic_MyFollow = new System.Windows.Forms.PictureBox();
            this.pic_Profil = new System.Windows.Forms.PictureBox();
            this.pic_Minimize = new System.Windows.Forms.PictureBox();
            this.pic_Close = new System.Windows.Forms.PictureBox();
            this.pic_CreateSeries = new System.Windows.Forms.PictureBox();
            this.lbl_ContentName = new System.Windows.Forms.Label();
            this.lbl_ContentType = new System.Windows.Forms.Label();
            this.lbl_ContentNbrEp = new System.Windows.Forms.Label();
            this.lbl_ContentNbrSeason = new System.Windows.Forms.Label();
            this.lbl_ContentProducer = new System.Windows.Forms.Label();
            this.Rtxt_ContentSynopsis = new System.Windows.Forms.RichTextBox();
            this.pic_ContentPicture = new System.Windows.Forms.PictureBox();
            this.txt_ContentType = new System.Windows.Forms.TextBox();
            this.txt_ContentnbrEpisode = new System.Windows.Forms.TextBox();
            this.txt_ContentNbrSeason = new System.Windows.Forms.TextBox();
            this.txt_ContentProducer = new System.Windows.Forms.TextBox();
            this.lbl_ContentSynopsis = new System.Windows.Forms.Label();
            this.btn_Logout = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pic_TSAMLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_News)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_MyFollow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Profil)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Minimize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Close)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_CreateSeries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_ContentPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_UserEmail
            // 
            this.lbl_UserEmail.AutoSize = true;
            this.lbl_UserEmail.Location = new System.Drawing.Point(669, 9);
            this.lbl_UserEmail.Name = "lbl_UserEmail";
            this.lbl_UserEmail.Size = new System.Drawing.Size(64, 13);
            this.lbl_UserEmail.TabIndex = 0;
            this.lbl_UserEmail.Text = "\"user email\"";
            // 
            // lbl_EMAIL
            // 
            this.lbl_EMAIL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_EMAIL.BackColor = System.Drawing.Color.Transparent;
            this.lbl_EMAIL.Location = new System.Drawing.Point(668, 59);
            this.lbl_EMAIL.Name = "lbl_EMAIL";
            this.lbl_EMAIL.Size = new System.Drawing.Size(129, 13);
            this.lbl_EMAIL.TabIndex = 0;
            this.lbl_EMAIL.Text = "qwertzu";
            this.lbl_EMAIL.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pic_TSAMLogo
            // 
            this.pic_TSAMLogo.BackColor = System.Drawing.Color.Transparent;
            this.pic_TSAMLogo.Image = ((System.Drawing.Image)(resources.GetObject("pic_TSAMLogo.Image")));
            this.pic_TSAMLogo.Location = new System.Drawing.Point(12, 0);
            this.pic_TSAMLogo.Name = "pic_TSAMLogo";
            this.pic_TSAMLogo.Size = new System.Drawing.Size(204, 81);
            this.pic_TSAMLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_TSAMLogo.TabIndex = 1;
            this.pic_TSAMLogo.TabStop = false;
            // 
            // pic_Search
            // 
            this.pic_Search.BackColor = System.Drawing.Color.Transparent;
            this.pic_Search.Image = ((System.Drawing.Image)(resources.GetObject("pic_Search.Image")));
            this.pic_Search.InitialImage = null;
            this.pic_Search.Location = new System.Drawing.Point(254, 12);
            this.pic_Search.Name = "pic_Search";
            this.pic_Search.Size = new System.Drawing.Size(55, 60);
            this.pic_Search.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_Search.TabIndex = 8;
            this.pic_Search.TabStop = false;
            this.pic_Search.Click += new System.EventHandler(this.pic_Search_Click);
            // 
            // pic_News
            // 
            this.pic_News.BackColor = System.Drawing.Color.Transparent;
            this.pic_News.Image = ((System.Drawing.Image)(resources.GetObject("pic_News.Image")));
            this.pic_News.InitialImage = null;
            this.pic_News.Location = new System.Drawing.Point(460, 12);
            this.pic_News.Name = "pic_News";
            this.pic_News.Size = new System.Drawing.Size(55, 60);
            this.pic_News.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_News.TabIndex = 9;
            this.pic_News.TabStop = false;
            // 
            // pic_MyFollow
            // 
            this.pic_MyFollow.BackColor = System.Drawing.Color.Transparent;
            this.pic_MyFollow.Image = ((System.Drawing.Image)(resources.GetObject("pic_MyFollow.Image")));
            this.pic_MyFollow.InitialImage = null;
            this.pic_MyFollow.Location = new System.Drawing.Point(357, 12);
            this.pic_MyFollow.Name = "pic_MyFollow";
            this.pic_MyFollow.Size = new System.Drawing.Size(55, 60);
            this.pic_MyFollow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_MyFollow.TabIndex = 10;
            this.pic_MyFollow.TabStop = false;
            // 
            // pic_Profil
            // 
            this.pic_Profil.BackColor = System.Drawing.Color.Transparent;
            this.pic_Profil.Image = ((System.Drawing.Image)(resources.GetObject("pic_Profil.Image")));
            this.pic_Profil.InitialImage = null;
            this.pic_Profil.Location = new System.Drawing.Point(758, 12);
            this.pic_Profil.Name = "pic_Profil";
            this.pic_Profil.Size = new System.Drawing.Size(39, 36);
            this.pic_Profil.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_Profil.TabIndex = 11;
            this.pic_Profil.TabStop = false;
            // 
            // pic_Minimize
            // 
            this.pic_Minimize.BackColor = System.Drawing.Color.Transparent;
            this.pic_Minimize.Image = ((System.Drawing.Image)(resources.GetObject("pic_Minimize.Image")));
            this.pic_Minimize.Location = new System.Drawing.Point(803, 0);
            this.pic_Minimize.Name = "pic_Minimize";
            this.pic_Minimize.Size = new System.Drawing.Size(47, 36);
            this.pic_Minimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_Minimize.TabIndex = 13;
            this.pic_Minimize.TabStop = false;
            this.pic_Minimize.Click += new System.EventHandler(this.pic_Minimize_Click);
            // 
            // pic_Close
            // 
            this.pic_Close.BackColor = System.Drawing.Color.Transparent;
            this.pic_Close.Image = ((System.Drawing.Image)(resources.GetObject("pic_Close.Image")));
            this.pic_Close.Location = new System.Drawing.Point(856, 0);
            this.pic_Close.Name = "pic_Close";
            this.pic_Close.Size = new System.Drawing.Size(48, 36);
            this.pic_Close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_Close.TabIndex = 12;
            this.pic_Close.TabStop = false;
            this.pic_Close.Click += new System.EventHandler(this.pic_Close_Click);
            // 
            // pic_CreateSeries
            // 
            this.pic_CreateSeries.BackColor = System.Drawing.Color.Transparent;
            this.pic_CreateSeries.Image = ((System.Drawing.Image)(resources.GetObject("pic_CreateSeries.Image")));
            this.pic_CreateSeries.Location = new System.Drawing.Point(568, 12);
            this.pic_CreateSeries.Name = "pic_CreateSeries";
            this.pic_CreateSeries.Size = new System.Drawing.Size(64, 60);
            this.pic_CreateSeries.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_CreateSeries.TabIndex = 14;
            this.pic_CreateSeries.TabStop = false;
            // 
            // lbl_ContentName
            // 
            this.lbl_ContentName.AutoSize = true;
            this.lbl_ContentName.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ContentName.Font = new System.Drawing.Font("Rockwell", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ContentName.Location = new System.Drawing.Point(84, 103);
            this.lbl_ContentName.Name = "lbl_ContentName";
            this.lbl_ContentName.Size = new System.Drawing.Size(242, 31);
            this.lbl_ContentName.TabIndex = 15;
            this.lbl_ContentName.Text = "Content To Toggle";
            // 
            // lbl_ContentType
            // 
            this.lbl_ContentType.AutoSize = true;
            this.lbl_ContentType.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ContentType.Font = new System.Drawing.Font("Rockwell", 9.75F);
            this.lbl_ContentType.Location = new System.Drawing.Point(9, 144);
            this.lbl_ContentType.Name = "lbl_ContentType";
            this.lbl_ContentType.Size = new System.Drawing.Size(44, 16);
            this.lbl_ContentType.TabIndex = 16;
            this.lbl_ContentType.Text = "Genre";
            // 
            // lbl_ContentNbrEp
            // 
            this.lbl_ContentNbrEp.AutoSize = true;
            this.lbl_ContentNbrEp.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ContentNbrEp.Font = new System.Drawing.Font("Rockwell", 9.75F);
            this.lbl_ContentNbrEp.Location = new System.Drawing.Point(9, 206);
            this.lbl_ContentNbrEp.Name = "lbl_ContentNbrEp";
            this.lbl_ContentNbrEp.Size = new System.Drawing.Size(120, 16);
            this.lbl_ContentNbrEp.TabIndex = 17;
            this.lbl_ContentNbrEp.Text = "Nombre d\'episodes";
            // 
            // lbl_ContentNbrSeason
            // 
            this.lbl_ContentNbrSeason.AutoSize = true;
            this.lbl_ContentNbrSeason.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ContentNbrSeason.Font = new System.Drawing.Font("Rockwell", 9.75F);
            this.lbl_ContentNbrSeason.Location = new System.Drawing.Point(9, 268);
            this.lbl_ContentNbrSeason.Name = "lbl_ContentNbrSeason";
            this.lbl_ContentNbrSeason.Size = new System.Drawing.Size(118, 16);
            this.lbl_ContentNbrSeason.TabIndex = 18;
            this.lbl_ContentNbrSeason.Text = "Nombre de saisons";
            // 
            // lbl_ContentProducer
            // 
            this.lbl_ContentProducer.AutoSize = true;
            this.lbl_ContentProducer.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ContentProducer.Font = new System.Drawing.Font("Rockwell", 9.75F);
            this.lbl_ContentProducer.Location = new System.Drawing.Point(9, 323);
            this.lbl_ContentProducer.Name = "lbl_ContentProducer";
            this.lbl_ContentProducer.Size = new System.Drawing.Size(114, 16);
            this.lbl_ContentProducer.TabIndex = 19;
            this.lbl_ContentProducer.Text = "Studio/Réalisateur";
            // 
            // Rtxt_ContentSynopsis
            // 
            this.Rtxt_ContentSynopsis.Enabled = false;
            this.Rtxt_ContentSynopsis.Font = new System.Drawing.Font("Rockwell", 9.75F);
            this.Rtxt_ContentSynopsis.Location = new System.Drawing.Point(16, 424);
            this.Rtxt_ContentSynopsis.Name = "Rtxt_ContentSynopsis";
            this.Rtxt_ContentSynopsis.Size = new System.Drawing.Size(341, 96);
            this.Rtxt_ContentSynopsis.TabIndex = 5;
            this.Rtxt_ContentSynopsis.Text = "";
            // 
            // pic_ContentPicture
            // 
            this.pic_ContentPicture.BackColor = System.Drawing.Color.Transparent;
            this.pic_ContentPicture.Image = ((System.Drawing.Image)(resources.GetObject("pic_ContentPicture.Image")));
            this.pic_ContentPicture.Location = new System.Drawing.Point(400, 129);
            this.pic_ContentPicture.Name = "pic_ContentPicture";
            this.pic_ContentPicture.Size = new System.Drawing.Size(320, 350);
            this.pic_ContentPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_ContentPicture.TabIndex = 21;
            this.pic_ContentPicture.TabStop = false;
            this.pic_ContentPicture.Click += new System.EventHandler(this.pic_ContentPicture_Click);
            // 
            // txt_ContentType
            // 
            this.txt_ContentType.Enabled = false;
            this.txt_ContentType.Font = new System.Drawing.Font("Rockwell", 9.75F);
            this.txt_ContentType.Location = new System.Drawing.Point(12, 163);
            this.txt_ContentType.Name = "txt_ContentType";
            this.txt_ContentType.Size = new System.Drawing.Size(123, 23);
            this.txt_ContentType.TabIndex = 1;
            // 
            // txt_ContentnbrEpisode
            // 
            this.txt_ContentnbrEpisode.Enabled = false;
            this.txt_ContentnbrEpisode.Font = new System.Drawing.Font("Rockwell", 9.75F);
            this.txt_ContentnbrEpisode.Location = new System.Drawing.Point(12, 225);
            this.txt_ContentnbrEpisode.Name = "txt_ContentnbrEpisode";
            this.txt_ContentnbrEpisode.Size = new System.Drawing.Size(123, 23);
            this.txt_ContentnbrEpisode.TabIndex = 2;
            // 
            // txt_ContentNbrSeason
            // 
            this.txt_ContentNbrSeason.Enabled = false;
            this.txt_ContentNbrSeason.Font = new System.Drawing.Font("Rockwell", 9.75F);
            this.txt_ContentNbrSeason.Location = new System.Drawing.Point(12, 287);
            this.txt_ContentNbrSeason.Name = "txt_ContentNbrSeason";
            this.txt_ContentNbrSeason.Size = new System.Drawing.Size(123, 23);
            this.txt_ContentNbrSeason.TabIndex = 3;
            // 
            // txt_ContentProducer
            // 
            this.txt_ContentProducer.Enabled = false;
            this.txt_ContentProducer.Font = new System.Drawing.Font("Rockwell", 9.75F);
            this.txt_ContentProducer.Location = new System.Drawing.Point(12, 342);
            this.txt_ContentProducer.Name = "txt_ContentProducer";
            this.txt_ContentProducer.Size = new System.Drawing.Size(123, 23);
            this.txt_ContentProducer.TabIndex = 4;
            // 
            // lbl_ContentSynopsis
            // 
            this.lbl_ContentSynopsis.AutoSize = true;
            this.lbl_ContentSynopsis.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ContentSynopsis.Font = new System.Drawing.Font("Rockwell", 9.75F);
            this.lbl_ContentSynopsis.Location = new System.Drawing.Point(13, 405);
            this.lbl_ContentSynopsis.Name = "lbl_ContentSynopsis";
            this.lbl_ContentSynopsis.Size = new System.Drawing.Size(59, 16);
            this.lbl_ContentSynopsis.TabIndex = 26;
            this.lbl_ContentSynopsis.Text = "Synopsis";
            // 
            // btn_Logout
            // 
            this.btn_Logout.BackColor = System.Drawing.SystemColors.Control;
            this.btn_Logout.Font = new System.Drawing.Font("Rockwell", 9.75F);
            this.btn_Logout.Location = new System.Drawing.Point(799, 51);
            this.btn_Logout.Name = "btn_Logout";
            this.btn_Logout.Size = new System.Drawing.Size(105, 30);
            this.btn_Logout.TabIndex = 27;
            this.btn_Logout.Text = "Deconnection";
            this.btn_Logout.UseVisualStyleBackColor = false;
            this.btn_Logout.Click += new System.EventHandler(this.btn_Logout_Click);
            // 
            // frm_LoggedToggleContent
            // 
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(903, 559);
            this.ControlBox = false;
            this.Controls.Add(this.btn_Logout);
            this.Controls.Add(this.lbl_ContentSynopsis);
            this.Controls.Add(this.txt_ContentProducer);
            this.Controls.Add(this.txt_ContentNbrSeason);
            this.Controls.Add(this.txt_ContentnbrEpisode);
            this.Controls.Add(this.txt_ContentType);
            this.Controls.Add(this.pic_ContentPicture);
            this.Controls.Add(this.Rtxt_ContentSynopsis);
            this.Controls.Add(this.lbl_ContentProducer);
            this.Controls.Add(this.lbl_ContentNbrSeason);
            this.Controls.Add(this.lbl_ContentNbrEp);
            this.Controls.Add(this.lbl_ContentType);
            this.Controls.Add(this.lbl_ContentName);
            this.Controls.Add(this.pic_CreateSeries);
            this.Controls.Add(this.pic_Minimize);
            this.Controls.Add(this.pic_Close);
            this.Controls.Add(this.pic_News);
            this.Controls.Add(this.pic_MyFollow);
            this.Controls.Add(this.pic_Search);
            this.Controls.Add(this.pic_Profil);
            this.Controls.Add(this.pic_TSAMLogo);
            this.Controls.Add(this.lbl_EMAIL);
            this.MaximizeBox = false;
            this.Name = "frm_LoggedToggleContent";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = " ";
            this.Load += new System.EventHandler(this.frm_Logged_Load);
            this.LocationChanged += new System.EventHandler(this.frm_Logged_LocationChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pic_TSAMLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_News)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_MyFollow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Profil)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Minimize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Close)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_CreateSeries)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_ContentPicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblConfirmedregistation;
        private System.Windows.Forms.Label lblCongratulation;
        private System.Windows.Forms.Button btnToLogin;
        private System.Windows.Forms.Label lbl_UserEmail;
        private System.Windows.Forms.Label lbl_EMAIL;
        private System.Windows.Forms.PictureBox pic_TSAMLogo;
        private System.Windows.Forms.PictureBox pic_Search;
        private System.Windows.Forms.PictureBox pic_News;
        private System.Windows.Forms.PictureBox pic_MyFollow;
        private System.Windows.Forms.PictureBox pic_Profil;
        private System.Windows.Forms.PictureBox pic_Minimize;
        private System.Windows.Forms.PictureBox pic_Close;
        private System.Windows.Forms.PictureBox pic_CreateSeries;
        private System.Windows.Forms.Label lbl_ContentName;
        private System.Windows.Forms.Label lbl_ContentType;
        private System.Windows.Forms.Label lbl_ContentNbrEp;
        private System.Windows.Forms.Label lbl_ContentNbrSeason;
        private System.Windows.Forms.Label lbl_ContentProducer;
        private System.Windows.Forms.RichTextBox Rtxt_ContentSynopsis;
        private System.Windows.Forms.PictureBox pic_ContentPicture;
        private System.Windows.Forms.TextBox txt_ContentType;
        private System.Windows.Forms.TextBox txt_ContentnbrEpisode;
        private System.Windows.Forms.TextBox txt_ContentNbrSeason;
        private System.Windows.Forms.TextBox txt_ContentProducer;
        private System.Windows.Forms.Label lbl_ContentSynopsis;
        private System.Windows.Forms.Button btn_Logout;
    }
}