﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TSAM.Model;

namespace TSAM.IHM
{
    public partial class frm_ValidateRegister : Form
    {
        
        public frm_ValidateRegister(string email)
        {
            //change the lblCongratulation.text to contain the user email.
            InitializeComponent();
            lblCongratulation.Text = email;

        }


        /// <summary>
        /// click on btnToLogin, this open Login form and close registerValidation form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnToLogin_Click(object sender, EventArgs e)
        {
            frm_Login f = new frm_Login();
            f.FormClosing += new FormClosingEventHandler(f_FormClosing);
            this.Hide();
            f.Show();
        }
        /// <summary>
        /// function designed to close the form after next form to open is loaded 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void f_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Load the configuration file to set the location of the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frm_ValidateRegister_Load(object sender, EventArgs e)
        {
           
                Config startConfig = new Config();
                startConfig.Load();
                this.Location = new Point(startConfig.app_Width, startConfig.app_Heigh);
            
        }

        /// <summary>
        /// when frm_RegisterValidation.localisation is modified then the actual localisation is saved on the config file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frm_ValidateRegister_LocationChanged(object sender, EventArgs e)
        {
            Config configloader = new Config();
            configloader.Load();
            configloader.app_Heigh = this.Location.Y;
            configloader.app_Width = this.Location.X;
            configloader.Save();
        }

        private void pic_Minimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pic_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
