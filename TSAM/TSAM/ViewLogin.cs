﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TSAM.Model;
using System.Drawing;

namespace TSAM.IHM
{

    public partial class frm_Login : Form

    {
        private int indexLastChange;
        private StringBuilder password = new StringBuilder();
        private TimerCallback timerCallback;
        private System.Threading.Timer timer;     

        public frm_Login()
        {
         
            InitializeComponent();
            timerCallback = new TimerCallback(Do);
        }

        /// <summary>
        ///  Load the configuration file to set the location of the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmLogin_Load(object sender, EventArgs e)
        {
            Config startConfig = new Config();
            startConfig.Load();
            this.Location = new Point(startConfig.app_Width, startConfig.app_Heigh);
            startConfig.Save();
        }

        /// <summary>
        /// when  click on btnToRegister, this open register form and close login form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnToRegister_Click(object sender, EventArgs e)
        {
            frm_Register f = new frm_Register();
            f.FormClosing += new FormClosingEventHandler(f_FormClosing);
            this.Hide();
            f.Show();
        }

        /// <summary>
        ///function designed to close the form after next form to open is loaded 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void f_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// when btn_ValidateLogin is pressed:
        /// check the email format
        /// check if the email and password are in the sqlDB
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_ValidateLogin_Click(object sender, EventArgs e)
        {
            User connectedUser = new User();
            RegexUtilities checkEMail = new RegexUtilities();

            //check the email entry format
            if (checkEMail.IsValidEmail(txt_EntryEmail.Text))
            {
                bool verification = true;
               // send login info to the model
                try
                {
                    
                    string inputPassword;
                    inputPassword = password.ToString();

                    connectedUser.Login(txt_EntryEmail.Text, inputPassword);
                    
                    
                    
                }
                //catch if the login info send are not in the db
                catch (InvalidCreditentialsException error)
                {
                    //if email or password is incorrect then :
                 
                    lbl_error.Text = "L'email ou le mot de passe est incorrect";
                    lbl_error.Visible = true;
                    verification = false;                                            
                }
                //catch any other error that may come to this point
                //it prevent the application from crashing if the connection to the DB is lost
                catch (Exception noConnected)
                {
                    //if an "other" error is catched
                   
                    lbl_error.Text = "Nos services sont actuellement en maintenance";
                    lbl_error.Visible = true;
                    verification = false;                                      
                }
                //if all the test of catch and if are passed then we can open the logged form
                if(verification == true)
                {
                    
                    lbl_error.Visible = false;

                    //true = admin false = classic user
                    bool userRank = connectedUser.IsAdminAccount;

                    if(userRank == false)
                    {
                        frm_Logged f = new frm_Logged(connectedUser);
                        f.FormClosing += new FormClosingEventHandler(f_FormClosing);
                        this.Hide();
                        f.Show();
                    }
                    else
                    {
                        frm_AdminLogged f = new frm_AdminLogged(connectedUser);
                        f.FormClosing += new FormClosingEventHandler(f_FormClosing);
                        this.Hide();
                        f.Show();
                    }
                    //we use txt_EntryEmail.text because we need it on the logged form
                                 
                }             
            }
            //if the email format isn't validate then 
            else
            {
                
                lbl_error.Text = "Veuillez entrez une adresse email valable";
                lbl_error.Visible = true;
            }
        }
 
        /// <summary>
        /// 
        /// </summary>
        /// <param name="state"></param>
        private void Do(object state)
        {
            int num = this.txt_EntryPSW.Text.Count();
            if (num > 0)
            {
                StringBuilder s = new StringBuilder(this.txt_EntryPSW.Text);
                s[num - 1] = '●';                                   //hide the last character after one second
                this.Invoke(new Action(() =>
                {
                    var cursorIndex = txt_EntryPSW.SelectionStart;   //get the cursor index
                    this.txt_EntryPSW.Text = s.ToString();           // update the textbox
                    this.txt_EntryPSW.SelectionStart = cursorIndex;  //reset the cursor to the current cursor
                    timer.Dispose();                                //destroy the timer
                    timer = null;
                }));
            }
        }
          
        /// <summary>
        /// when frm_login.localisation is modified then the actual localisation is saved on the config file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frm_Login_LocationChanged(object sender, EventArgs e)
        {
            Config configloader = new Config();
            configloader.Load();
            configloader.app_Heigh = this.Location.Y;
            configloader.app_Width = this.Location.X;
            configloader.Save();
        }

        /// <summary>
        ///  when the user is writing in the password field then the caracter must hide after 1 seconde
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_EntryPSW_TextChanged(object sender, EventArgs e)
        {
            StringBuilder input = new StringBuilder(txt_EntryPSW.Text); //set the input of the textbox to a string builder
            var cursorIndex = txt_EntryPSW.SelectionStart;   // Get the starting position of the cursor
            for (int i = 0; i < txt_EntryPSW.Text.Count(); i++)   //Go through the textbox's characters
            {
                if (input[i] != '●')
                {
                    if (i >= password.Length)
                    {
                        password.Append(input[i]);
                    }
                    else
                    {
                        if (input.Length > password.Length)
                        {
                            password.Insert(i, input[i]); //insert if a letter has been typed in the middle of the text into the clear stringBuilder
                        }
                        else
                        {
                            password[i] = txt_EntryPSW.Text.ElementAt(i); //replace the letter
                        }
                    }
                    if (indexLastChange == i && input.Length > 0) //replace the clear character in the textbox
                    {
                        input[i] = '●';
                    }
                    indexLastChange = i; //Update the last index of a character that has changed
                }
            }
            int dif = password.Length - input.Length;
            if (dif > 0)
            {
                password.Remove(cursorIndex, dif); //remove a certain number of numbers in the clear password if have removed one or more characters
            }
            txt_EntryPSW.Text = input.ToString(); //update the textbox
            Console.WriteLine("password " + password); //show the password in console
            if (timer == null)
            {
                timer = new System.Threading.Timer(timerCallback, null, 1000, 0); //create  a new timer if the old one has been destroyed
            }
            else
            {
                timer.Change(1000, 0);  //reset the timer to 1000ms
            }
            int num = this.txt_EntryPSW.Text.Count();
            if (num > 1)
            {
                StringBuilder s = new StringBuilder(this.txt_EntryPSW.Text);
                s[num - 2] = '●'; //replace the before to last character to a star
                this.txt_EntryPSW.Text = s.ToString();
                this.txt_EntryPSW.SelectionStart = num;
            }
            this.txt_EntryPSW.SelectionStart = (cursorIndex > 0) ? cursorIndex : 0;   //third operator , test if the cursor is negative , it will go back to 0
            Console.WriteLine("Last index change :" + (indexLastChange)); //show the index in the console
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

    
    }
}        