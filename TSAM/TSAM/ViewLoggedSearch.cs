﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TSAM.Model;

namespace TSAM.IHM
{
    public partial class frm_LoggedSearch : Form
    {
        User connectedUser;
        List<object> theList = new List<object>();

        public frm_LoggedSearch(User connectedUser)
        {
            this.connectedUser = connectedUser;
            //change the lbl_EMAIL.text to contain the user email.
            InitializeComponent();
            lbl_EMAIL.Text = connectedUser.Mail;
        }





        /// <summary>
        /// when frm_Logged.localisation is modified then the actual localisation is saved on the config file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frm_Logged_LocationChanged(object sender, EventArgs e)
        {
            Config configloader = new Config();
            configloader.Load();
            configloader.app_Heigh = this.Location.Y;
            configloader.app_Width = this.Location.X;
            configloader.Save();
        }

        /// <summary>
        /// Load the configuration file to set the location of the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frm_Logged_Load(object sender, EventArgs e)
        {
            Config startConfig = new Config();
            startConfig.Load();
            this.Location = new Point(startConfig.app_Width, startConfig.app_Heigh);


        }

   

     

        /// <summary>
        /// close the form when another form is being loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void f_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Close();
        }


        /// <summary>
        /// minimize the current form when the minimize button is clicked on
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pic_Minimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        /// <summary>
        /// close the current form when the closing button is clicked on
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pic_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pic_Search_Click(object sender, EventArgs e)
        {

        }

       

        private void btn_Search_Click(object sender, EventArgs e)
        {
            lst_Collection.Items.Clear();

            
            
                Collection collection = new Collection();

                collection.Search(txtBox_SearchContent.Text);

                foreach(Content content in collection.Contents)
                {
                    lst_Collection.Items.Add(content);
                }

            
            

        }

        private void btn_ToggleContent_Click(object sender, EventArgs e)
        {
            frm_LoggedToggleContent f = new frm_LoggedToggleContent(connectedUser, (Content)lst_Collection.SelectedItem);
            f.FormClosing += new FormClosingEventHandler(f_FormClosing);
            this.Hide();
            f.Show();
        }

        private void btn_Logout_Click(object sender, EventArgs e)
        {
            frm_Login f = new frm_Login();
            f.FormClosing += new FormClosingEventHandler(f_FormClosing);
            this.Hide();
            f.Show();
        }

        private void lst_Collection_SelectedIndexChanged(object sender, EventArgs e)
        {
            pictureBox1.Visible = true;
            try
            {
                Content item = (Content)lst_Collection.SelectedItem;
                pictureBox1.Load(item.ImageUrl);
            }

            catch(System.Net.WebException)
            {
                
            }
        }
    }

      
}
