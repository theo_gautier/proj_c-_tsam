﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TSAM.Model
{
    /// <summary>
    /// this testClass tests the user class
    /// </summary>
    [TestClass]
    public class TestUser
    {
        /// <summary>
        /// this test class tests the register
        /// IMPORTANT: This test must be runned before the login test
        /// </summary>
        [TestMethod]
        public void User_Register_Success()
        {
            //given
            string mail = "testmethod@test.ch";
            string password = "testmethod@test.ch";
            User user = new User();
            string expectedEmail = mail;
            string actualEmail;


            user.Register(mail, password);
            user = new User();
            user.Login(mail, password);
            actualEmail = user.Mail;

            Assert.AreEqual(expectedEmail, actualEmail);
        }

        /// <summary>
        /// this test tests the login
        /// IMPORTANT: the Register test must be runned before this test if you use the same creditentials
        /// </summary>
        [TestMethod]
        public void User_Login_Success()
        {
            //given
            string mail = "testmethod@test.ch";
            string password = "testmethod@test.ch";
            User user = new User();
            string expectedEmail = mail;
            string actualEmail;

            user.Login(mail, password);
            actualEmail = user.Mail;

            Assert.AreEqual(expectedEmail, actualEmail);
        }

       

        
    }
}
