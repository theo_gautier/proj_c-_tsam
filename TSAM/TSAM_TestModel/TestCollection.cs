﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TSAM.Model
{
    
    [TestClass]
    public class TestCollection
    {
        /// <summary>
        /// this test method tests the load mathod. Be careful, an anime or a serie have to exists in the database. 
        /// </summary>
        [TestMethod]
        public void Collection_Load_success()
        {
            #region given
            Collection collection = new Collection();
            #endregion

            #region when
            collection.Load();
            #endregion

            #region then
            Assert.IsNotNull(collection.Contents);
            #endregion
        }

        /// <summary>
        /// this test method tests the search mathod. Be careful, the anime or the serie searched have to exists in the database. 
        /// </summary>
        [TestMethod]
        public void Collection_search_success()
        {
            #region given
            Collection collection = new Collection();
            string searchedTitle = "Mr.Robot";
            #endregion

            #region when
            collection.Search("Mr.Robot");
            #endregion

            #region then
            Assert.IsNotNull(collection.Contents);
            #endregion
        }

    }
}
