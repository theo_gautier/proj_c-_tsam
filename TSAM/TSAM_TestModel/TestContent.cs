﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TSAM.Model
{
    /** \brief This test class tests the content
     */
    [TestClass]
    public class TestContent
    {
        /** \brief The test method tests the create method. it use the search method, this method have to work before.
         */

       
        [TestMethod]
        public void Content_create_Success()
        {
            //given

            Content content = new Content();
            Collection collection = new Collection();
            string expectedName = "Mr.Robot";
            string expectedType = "cyber";
            int expectedNbEpisodes = 45;
            int expectedNbSaisons = 4;
            string expectedProducer = "Anonymous Content";
            bool expectedIsAnime = false;
            string expectedSynopsis = "a serie";
            string actualSynopsis="";
            string imageUrl = "";



            //when

            content.Create(expectedName, expectedType, expectedNbEpisodes, expectedNbSaisons, expectedProducer, expectedIsAnime, expectedSynopsis, imageUrl);

            collection.Search(expectedName);

            actualSynopsis = collection.Contents[0].Synopsis;

            //then

            Assert.AreEqual(expectedSynopsis, actualSynopsis);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void Content_urlImageAtCreate_Exception()
        {
            //given

            Content content = new Content();
            Collection collection = new Collection();
            string name = "Shooter";
            string type = "militaire";
            int episodes = 31;
            int nbSaisons = 3;
            string producer = "	John Hlavin";
            bool isAnime = false;
            string synopsis = "serie militaire/de guerre";
            string imageUrl = "https://upload.wikimedia.org/wikipedia/commons/8/89/Banshee.jpg";

            // when
            
            
                content.Create(name, type, episodes, nbSaisons, producer, isAnime, synopsis, imageUrl);
            
           


        }
    }
}
