﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;


namespace TSAM.Model
{
    /// <summary>
    /// this test class tests the config class
    /// </summary>
    [TestClass]
    public class TestConfig
    {

        /// <summary>
        /// This TestMethod will test if the config can load itself from a json
        /// </summary>
        [TestMethod]
        public void Config_Load_Success()
        {
            #region Given
            Config config = new Config();
            string expectedLanguage = "FR";
            string actualLanguage = "";
            #endregion

            #region when
            config.Load();
            actualLanguage = config.app_Language;
            #endregion

            #region then
            Assert.AreEqual(expectedLanguage, actualLanguage);
            #endregion
        }

        /// <summary>
        /// This TestMethod will test if the config can save
        /// </summary>
        [TestMethod]
        public void Config_Save_Success()
        {
            #region Given
            Config config = new Config();
            string filePath = @"" + Environment.GetEnvironmentVariable("appdata") + "\\LoginProject\\userPreferances.json";
        #endregion

        #region when
            config.Load();
            config.Save();
            string originalText = File.ReadAllText(filePath);
            config.app_Heigh += 1;
            config.Save();
            string finalText = File.ReadAllText(filePath);
            #endregion

            #region then
            Assert.AreNotEqual(originalText, finalText);
            #endregion
        }
    }
}
